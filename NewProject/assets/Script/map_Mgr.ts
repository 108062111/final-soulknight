const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    enter_boss_room: boolean = false;
    play_boss_anim: boolean = false;

    remainTime: number = 0;
    timeText: cc.Label;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    menubgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    entereffect: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    buttoneffect: cc.AudioClip = null;

    start(){
        this.timeText = cc.find("timer").getComponent(cc.Label);
        this.playBGM();
        
        //為了fail做準備
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                let Email: string = user.email.replace(/\./gi,"");
                var finduser = firebase.database().ref('user').child(Email);
                finduser.once('value').then(function(snapshot) {
                    var childData = snapshot.val();
                    cc.find("Canvas/Main Camera/fail/playerstate").getComponent(cc.Label).string = childData.playerstate.toString();
                }).catch(e => console.log(e.message));
            }
        });
        /*測試用
        this.scheduleOnce(function() {
            this.show_fail();
        }, 3);
        */

        /*測雪人進場動畫用
        this.scheduleOnce(function() {
            this.snow_anim();
        }, 1);
        */
        
    }
    snow_anim(){
        //變數
        this.play_boss_anim = true;
        cc.find("Canvas/knight1").getComponent('playermove').stop = true;
        let finish = cc.callFunc(function(target, tmp) {
            this.play_boss_anim = false;
            cc.find("Canvas/knight1").getComponent('playermove').stop = false;
            cc.find("Canvas/Main Camera/play_boss_anim").active = false;
        }, this, 0);

        //blocking_up
        let up = cc.sequence(cc.delayTime(0.2), cc.moveTo(0.3, 46.355, 397.682), cc.delayTime(2), cc.moveTo(0.5, 40.878, 335.074), cc.moveTo(0.5, 64.877, 609.38));
        cc.find("Canvas/Main Camera/play_boss_anim/blocking_up").runAction(up);

        //blocking_down
        let down = cc.sequence(cc.delayTime(0.2), cc.moveTo(0.3, -23.789, -404.072), cc.delayTime(2), cc.moveTo(0.5, -17.932, -337.131), cc.moveTo(0.5, -41.693, -608.723));
        cc.find("Canvas/Main Camera/play_boss_anim/blocking_down").runAction(down);

        //string
        let str = cc.sequence(cc.delayTime(0.5), cc.moveTo(0.4, -264.706, 107.598), cc.moveTo(0.3, -122.528, 90.141), cc.delayTime(1), cc.moveTo(0.5, -365.69, 119.998).easing(cc.easeInOut(2)), cc.moveTo(0.5, 810.003, -24.359));
        cc.find("Canvas/Main Camera/play_boss_anim/string").runAction(str);

        //boss
        let boss = cc.sequence(cc.delayTime(0.5), cc.moveTo(0.4, 218.14, -19.087), cc.moveTo(0.3, 313.638, -30.813), cc.delayTime(1), cc.moveTo(0.5, 223.913, -19.796).easing(cc.easeInOut(2)), cc.moveTo(0.5, 883.378, -100.769));
        cc.find("Canvas/Main Camera/play_boss_anim/boss").runAction(boss);

        //整體node
        let all = cc.sequence(cc.fadeIn(0.2), cc.delayTime(3), finish);
        cc.find("Canvas/Main Camera/play_boss_anim").runAction(all);
        
    }
    onLoad () {
        let arrow = cc.repeatForever(cc.sequence(cc.moveBy(0.4, 0, -10), cc.moveBy(0.4, 0, 10)));
        cc.find("Canvas/map/magic/magic_grid/arrow").runAction(arrow);
    }

    playBGM(){
        cc.audioEngine.playMusic(this.bgm, true);
    }

    stopBGM(){
        cc.audioEngine.pauseMusic();
    }

    //按下magic_grid就會開啟record!
    Button_magic_grid (event, customEventData) {
        cc.log("press magic grid");
        cc.find("Canvas/map/magic/magic_grid").active = false;
        cc.find("Canvas/map/magic/magic_grid_anim").active = true;
        //先把時間寫到record上
        let timer:number = Number(this.remainTime.toFixed(0));
        let tmp1: number = timer/60;
        let tmp2: number = timer/3600;
        let sec:number = timer%60;
        let min:number = Number(tmp1.toFixed(0))%60;
        let hour:number = Number(tmp2.toFixed(0));
        cc.log("timer = "+hour+": "+min+": "+sec);
        cc.find("Canvas/Main Camera/record/hour").getComponent(cc.Label).string = hour.toString();
        cc.find("Canvas/Main Camera/record/min").getComponent(cc.Label).string = min.toString();
        cc.find("Canvas/Main Camera/record/sec").getComponent(cc.Label).string = sec.toString();
        //日期
        var date = new Date();
        let year = date.getFullYear()%100;
        let month = date.getMonth() + 1;
        cc.find("Canvas/Main Camera/record/year").getComponent(cc.Label).string = year.toString();
        cc.find("Canvas/Main Camera/record/month").getComponent(cc.Label).string = month.toString();
        cc.find("Canvas/Main Camera/record/date").getComponent(cc.Label).string = date.getDate().toString();
        //填上名字
        firebase.auth().onAuthStateChanged(function(user) {
            // Check user login
            if (user) {
                let Email: string = user.email.replace(/\./gi,"");
                //用Email撈firebase其他資料填上
                var finduser = firebase.database().ref('user').child(Email);
                finduser.once('value').then(function(snapshot) {
                    var childData = snapshot.val();
                    cc.find("Canvas/Main Camera/record/username").getComponent(cc.Label).string = childData.username;
                    //決定這次是要用knight還是wizard的圖
                    if(childData.playerstate == 0){
                        cc.find("Canvas/Main Camera/record/knight").active = true;
                    }else if(childData.playerstate == 1){
                        cc.find("Canvas/Main Camera/record/wizard").active = true;
                    }
                    //如果從來沒紀錄，更新firebase
                    if(childData.record == 0){
                        cc.log("從來沒紀錄");
                        var data = {
                            record: 1, 
                            time: timer, 
                            year: year, 
                            month: month, 
                            date: date.getDate(), 
                            record_playerstate: childData.playerstate
                        };
                        finduser.update(data);
                    }
                    //如果有紀錄，要比較誰快
                    else if(childData.record == 1){
                        cc.log("有紀錄，要比較");
                        if(childData.time >= timer){
                            var data2 = {
                                time: timer, 
                                year: year, 
                                month: month, 
                                date: date.getDate(), 
                                record_playerstate: childData.playerstate
                            };
                            finduser.update(data2);
                        }
                    }
                }).catch(e => console.log(e.message));
                //(結束)用Email撈其他資料填上
            }
        });
        //circle動畫
        let open_record = cc.callFunc(function(target, tmp) {
            cc.find("Canvas/Main Camera/record").active = true;
        }, this, 0);
        let act1 = cc.spawn(cc.fadeOut(1), cc.scaleTo(1, 1));
        let act2 = cc.sequence(
            cc.delayTime(0.3),
            cc.fadeIn(0),
            cc.spawn(cc.fadeOut(1), cc.scaleTo(1, 1))   
        );
        let act3 = cc.sequence(
            cc.delayTime(0.6),
            cc.fadeIn(0),
            cc.spawn(cc.fadeOut(2.2), cc.scaleTo(2.2, 2)), 
            open_record   
        );
        let white = cc.sequence(
            cc.delayTime(1.2),
            cc.fadeIn(3)  
        );
        cc.find("Canvas/Main Camera/white").runAction(white);
        cc.find("Canvas/map/magic/magic_grid_anim/1").runAction(act1);
        cc.find("Canvas/map/magic/magic_grid_anim/2").runAction(act2);
        cc.find("Canvas/map/magic/magic_grid_anim/3").runAction(act3);
    }
    //record的close
    Button_close (event, customEventData) {
        cc.log("press record's close");
        cc.audioEngine.playEffect(this.entereffect, false);
        cc.audioEngine.playMusic(this.menubgm, true);
        //todo 要換場景
        cc.director.loadScene("Select");
    }
    fail: boolean = false;
    show_fail(){
        this.fail = true;
        //先把時間寫到record上
        let timer:number = Number(this.remainTime.toFixed(0));
        let tmp1: number = timer/60;
        let tmp2: number = timer/3600;
        let sec:number = timer%60;
        let min:number = Number(tmp1.toFixed(0))%60;
        let hour:number = Number(tmp2.toFixed(0));
        cc.log("timer = "+hour+": "+min+": "+sec);
        cc.find("Canvas/Main Camera/fail/hour").getComponent(cc.Label).string = hour.toString();
        cc.find("Canvas/Main Camera/fail/min").getComponent(cc.Label).string = min.toString();
        cc.find("Canvas/Main Camera/fail/sec").getComponent(cc.Label).string = sec.toString();
        cc.find("Canvas/Main Camera/fail").active = true;
        //如果是knight
        if(cc.find("Canvas/Main Camera/fail/playerstate").getComponent(cc.Label).string == "0"){
            cc.find("Canvas/Main Camera/fail/knight").active = true;
            cc.find("Canvas/Main Camera/fail/knight").getComponent(cc.Animation).play('knight_walk');
            let rundead = cc.callFunc(function(target, tmp) {
                cc.find("Canvas/Main Camera/fail/knight").getComponent(cc.Animation).play('knight_dead');
            }, this, 0);
            let knight = cc.sequence(cc.moveBy(2, 530, 0), rundead);
            cc.find("Canvas/Main Camera/fail/knight").runAction(knight);
        }
        //如果是wizard
        else if(cc.find("Canvas/Main Camera/fail/playerstate").getComponent(cc.Label).string == "1"){
            cc.find("Canvas/Main Camera/fail/wizard").active = true;
            cc.find("Canvas/Main Camera/fail/wizard").getComponent(cc.Animation).play('wizard_walk');
            let rundead = cc.callFunc(function(target, tmp) {
                cc.find("Canvas/Main Camera/fail/wizard").getComponent(cc.Animation).play('wizard_dead');
            }, this, 0);
            let wizard = cc.sequence(cc.moveBy(2, 530, 0), rundead);
            cc.find("Canvas/Main Camera/fail/wizard").runAction(wizard);
        }

    }
    update(dt) {
        this.updatetime(dt);
        if(this.enter_boss_room == true){
            this.enter_boss_room = false;
            this.snow_anim();
        }
        if(!this.fail && (cc.find("Canvas/Main Camera/Player_status_box/hp").getComponent(cc.Label).string == "0/5" || cc.find("Canvas/Main Camera/Player_status_box/hp").getComponent(cc.Label).string == "0/3")){
            this.show_fail();
        }
        
    }
    updatetime(dt){
        this.remainTime += dt;
        this.timeText.string = this.remainTime.toFixed(0).toString();
    }
    //fail的enter
    Button_fail_enter (event, customEventData) {
        cc.log("press fail's enter button");
        cc.audioEngine.playEffect(this.buttoneffect, false);
        cc.audioEngine.playMusic(this.menubgm, true);
        //todo 要換場景
        cc.director.loadScene("Select");
    }
}
