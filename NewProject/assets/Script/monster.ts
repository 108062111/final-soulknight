import AStar from "./AStar";
import MapCell, { ECellType } from "./MapCell";

const {ccclass, property} = cc._decorator;

enum MonsterState {
    Idle,
    Stroll,
    Tracking,
    Attack,
    Die
}

@ccclass
export class monster extends cc.Component {
    @property({type:cc.AudioClip})
    attackSE: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    deadSE: cc.AudioClip = null;

    @property(cc.Prefab)
    coinPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    energyPrefab: cc.Prefab = null;

    monsterState: number = MonsterState.Idle;

    hp: number = 10;

    speed: number = 50;

    trackDist: number = 300;

    attackDist: number = 200;

    coin: number = 3;

    remainTime: number = 0;

    anim: cc.Animation = null;

    myClass: string = "";

    isStart: boolean = false; // todo: create a room to manage monster

    seeTarget: boolean = false;

    player: cc.Node = null;

    fail: cc.Node = null;

    @property(cc.Prefab)
    eCell: cc.Prefab = null;

    @property(cc.Node)
    eLayout: cc.Node = null;

    @property
    layoutWidth: number = 1;

    @property
    layoutHeight: number = 1;

    @property
    startX: number = 1;

    @property
    startY: number = 1;

    star = new AStar();
    mCells = {};

    mSize: cc.Vec2 = null;
    mStart: cc.Vec2 = null;
    mEnd: cc.Vec2 = null;
    @property([cc.Vec2])
    mObstacles: cc.Vec2[] = [];
    mPos: cc.Vec2 = null;

    strollCD: number = 0.5;
    strolltiming: number = 0;
    updatePathThreshold: number = 5;

    attackCD: number = 1.5;
    attacktiming: number = 0;

    iceCD: number = 2;
    isIce: boolean = false;

    onLoad () {
        // console.log("strollCD : ", this.strollCD);
        this.anim = this.getComponent(cc.Animation);
        this.player = cc.find("Canvas/knight1");
        this.fail = cc.find("Canvas/Main Camera/fail");

        const size = cc.v2(this.layoutWidth, this.layoutHeight);
        const start = cc.v2(this.startX, this.startY);
        // const end = cc.v2(this.layoutWidth-1, this.layoutHeight-1);
        // const end = cc.v2(0, 0);
        const end = start;
        /*const obstacles = [
            cc.v2(16,9)            
        ];*/

        this.init(size, start, end);
    }

    start () {

    }

    update (dt) {
        if(this.fail.active || this.monsterState == MonsterState.Die) return;
        this.remainTime += dt;
        // console.log(this.monsterState);
        if(this.isIce) return;
        if(this.monsterState == MonsterState.Idle){
            this.Idle();
        }
        else if(this.monsterState == MonsterState.Stroll){
            this.Stroll();
        }
        else if(this.monsterState == MonsterState.Tracking){
            this.Tracking();
        }
        else if(this.monsterState == MonsterState.Attack){
            this.Attack();
        }
    }

    onBeginContact(contact, self, other) {
        /* TODO: 
        1. bullet's tag
        2. - bullet's power
        */
        // cc.log("monster hit " + other.node.name);
        if(other.tag == 0) {
        }
        else if(other.tag == 5) { // player bullet
            this.BeAttack(1);
        }
        else if(other.tag == 6) { // player rainbow
            this.BeAttack(0.02);
        }
        else if(other.tag == 7) { // player(wizard) small ice
            this.BeAttack(1);
            this.isIce = true;
            this.anim.stop();
            this.node.stopAllActions();
            // console.log("ice");
            this.scheduleOnce(function() {
                this.isIce = false;
            }, this.iceCD);
        }
    }

    BeAttack (data: number){
        this.hp -= data;
        if (this.monsterState != MonsterState.Die && this.hp <= 0){
            this.monsterState = MonsterState.Die;
            this.node.parent.parent.getComponent("small_room").dead_num += 1;
            this.anim.play(this.myClass + "_dead");
            cc.audioEngine.playEffect(this.deadSE,false);
            let map = cc.find("Canvas/map");
            this.scheduleOnce(function() {
                for (let i = 0; i < this.coin; i++){
                    let pos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
                    pos = map.convertToNodeSpaceAR(cc.v2(pos.x+Math.random()*60-30, pos.y+Math.random()*60-30));
                    
                    let coin = cc.instantiate(this.coinPrefab);
                    coin.position = pos;
                    coin.parent = map;
                }
                for (let i = 0; i < this.coin; i++){
                    let pos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
                    pos = map.convertToNodeSpaceAR(cc.v2(pos.x+Math.random()*60-30, pos.y+Math.random()*60-30));
                    
                    let energy = cc.instantiate(this.energyPrefab);
                    energy.position = pos;
                    energy.parent = map;
                }
                this.node.parent.destroy();
            }, 0.5);
        }
    }

    Idle(){
        // todo
        if (this.node.parent.parent.getComponent("small_room").first_enter) {
        // if (this.dist(this.node.convertToWorldSpaceAR(cc.v2(0,0)), this.player.convertToWorldSpaceAR(cc.v2(0,0))) < 400) {
            // console.log("idle to stroll");
            this.star.RandomPath();
            this.updateStart(this.star.getStart());
            this.updateEnd(this.star.getEnd());

            this.strolltiming = this.remainTime;
            this.monsterState = MonsterState.Stroll;
        }
    }

    dist(a: cc.Vec2,  b: cc.Vec2){
        let dx = a.x - b.x;
        let dy = a.y - b.y;
        return Math.sqrt(dx*dx+dy*dy);
    }

    ManhattanDist(a: cc.Vec2,  b: cc.Vec2){
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }

    RaycastDetection(dist: number){
        let results=cc.director.getPhysicsManager().rayCast(this.node.convertToWorldSpaceAR(cc.v2(0,0)),this.player.convertToWorldSpaceAR(cc.v2(0,0)),cc.RayCastType.All);  
        let playdis = 200000000, mapdis = 200000000;
        for(let j=0;j<results.length;j++){
            if(results[j].collider.tag == 0){
                if(mapdis > results[j].fraction)
                    mapdis = results[j].fraction;
            }
            else if(results[j].collider.tag == 1){
                playdis = results[j].fraction;
            }
        }
        if(playdis != 200000000 && playdis < mapdis){
            if(this.dist(this.node.convertToWorldSpaceAR(cc.v2(0,0)), this.player.convertToWorldSpaceAR(cc.v2(0,0))) < dist) this.seeTarget = true;
            else this.seeTarget = false;    
        }
        else this.seeTarget = false;    
        
        // hit = Physics2D.Raycast(transform.position + Vector3.up, (targetPosition.position - (transform.position + Vector3.up)).normalized, trackingRange, layerMask);
        // if (hit.transform != null && hit.transform == targetPosition){
        //     seeTarget = true;
        //     Debug.DrawLine(transform.position + Vector3.up, hit.transform.position, Color.red);
        // }
        // else{ seeTarget = false; }
    }

    Stroll(){
        this.RaycastDetection(this.trackDist);
        if(this.seeTarget){
            if(this.myClass == "pig"){
                if(this.remainTime - this.attacktiming > this.attackCD){
                    this.monsterState = MonsterState.Tracking;
                    return;
                }
            }
            else{
                this.monsterState = MonsterState.Tracking;
                return;
            }
        }

        if (this.remainTime - this.strolltiming >= this.strollCD){
            this.strolltiming = this.remainTime;
            let nextTargetPosition = this.star.nextTarget();
            if(nextTargetPosition) this.UpdateLookAt(nextTargetPosition);
            else{
                // console.log("reach the end, find the next random path");
                this.anim.play(this.myClass + "_idle");
                this.star.RandomPath();
                this.updateStart(this.star.getStart());
                this.updateEnd(this.star.getEnd());
            }
        }

        // 陽春版Stroll
        // console.log(this.node.getComponent(cc.RigidBody).linearVelocity);
        // if(this.node.getComponent(cc.RigidBody).linearVelocity.equals(cc.v2(0,0))){
        //     let x = Math.random();
        //     let y = Math.sqrt(1 - x*x);
        //     let v = cc.v2(x,y);
        //     // let v = cc.v2(Math.random(), Math.random());
        //     // v.normalizeSelf();
        //     v.scaleSelf(cc.v2(this.speed,this.speed));
        //     console.log(v);
        //     this.node.getComponent(cc.RigidBody).linearVelocity = v;
        // }
    }

    UpdateLookAt(cell: cc.Vec2){
        this.mPos = cell;
        this.setCell(cell.x, cell.y, ECellType.VISITED);
        let pos = this.mCells[`${cell.x}_${cell.y}`].node.convertToWorldSpaceAR(cc.v2(0, 0));
        pos = this.node.parent.convertToNodeSpaceAR(cc.v2(pos.x, pos.y));

        // check scale
        if(pos.x - this.node.x > 3){ // move to right
            if(this.node.scaleX < 0){
                this.node.scaleX *= -1;
            }
        }
        else if(pos.x - this.node.x < -3){ // move to left
            if(this.node.scaleX > 0){
                this.node.scaleX *= -1;
            }
        }

        let action = cc.moveTo(this.strollCD, pos.x, pos.y); 
        this.node.runAction(action);
        this.anim.play(this.myClass + "_walk");
    }

    Tracking(){
        // find the player position in the cell coordinate
        let pos = this.player.convertToWorldSpaceAR(cc.v2(0,0)).sub(this.eLayout.convertToWorldSpaceAR(cc.v2(0,0)));
        pos = cc.v2(Math.floor((pos.x/2)/16),-Math.ceil((pos.y/2)/16));

        // find the new goal and update the path
        if(this.ManhattanDist(pos, this.mEnd) >= this.updatePathThreshold){
            if( (pos.x >= 0 && pos.x < this.layoutWidth) &&
                (pos.y >= 0 && pos.y < this.layoutHeight)){
                this.anim.play(this.myClass + "_idle");
                this.star.setNewStartEnd(this.mPos, pos);
                this.updateStart(this.mPos);
                this.updateEnd(pos);
            }
            else{
                // console.log("player out of the room");
            }
        }

        // myAI.NextTarget();
        // if (Vector2.Distance(transform.position, targetPosition.position) <= attackRange){
        //     monsterState = MonsterState.Attack;
        //     animator.SetBool("run", false);
        // }

        // update monster position
        if (this.remainTime - this.strolltiming >= this.strollCD){
            this.strolltiming = this.remainTime;
            let nextTargetPosition = this.star.nextTarget();
            if(nextTargetPosition) this.UpdateLookAt(nextTargetPosition);
            else{
                // console.log("reach the end, find the next random path");
                this.anim.play(this.myClass + "_idle");
                this.star.RandomPath();
                this.updateStart(this.star.getStart());
                this.updateEnd(this.star.getEnd());
            }
        }

        // update monster state
        if(this.monsterState == MonsterState.Tracking){
            this.RaycastDetection(this.attackDist);
            if(this.seeTarget){
                // console.log("tracking to attack");
                this.anim.play(this.myClass + "_idle");
                this.monsterState = MonsterState.Attack;
                return;
            }

            this.RaycastDetection(this.trackDist);
            if(!this.seeTarget){
                // console.log("tracking to stroll");
                this.monsterState = MonsterState.Stroll;
                return;
            }
        }

        // 陽春版Tracking
        // let v = this.player.position.sub(this.node.position);
        // v.normalizeSelf();
        // v.scaleSelf(cc.v2(this.speed,this.speed));
        // this.node.getComponent(cc.RigidBody).linearVelocity = v;

        // UpdateLookAt(myAI.nextTargetPosition);
    }

    Attack(){
        // console.log("monster attack");
    }

    init(size: cc.Vec2, start: cc.Vec2, end: cc.Vec2) { // , obstacles: cc.Vec2[] = []
        this.mSize = size;
        this.mStart = start;
        this.mEnd = end;
        this.mPos = start;
        // this.mObstacles = obstacles;
        
        this.initData();
        this.initUI();
    }
    
    initData() {
        this.star.init(this.mSize, this.mStart, this.mEnd, this.mObstacles);
    }
    
    initUI() {
        
        this.mCells = {};
        this.eLayout.removeAllChildren();
        for (let y = 0; y < this.mSize.y; y++){
            for (let x = 0; x < this.mSize.x; x++) {
                this.createCell(x, y);
            }
        }
    
        this.mObstacles.forEach((ele) => {
            this.setCell(ele.x, ele.y, ECellType.OBSTACLES);
        });
        const path = this.star.getClose();
        path.forEach((ele) => {
            this.setCell(ele.x, ele.y, ECellType.PATH);
        });
        this.setCell(this.mStart.x, this.mStart.y, ECellType.START);
        this.setCell(this.mEnd.x, this.mEnd.y, ECellType.END);
    }

    // 创建cell
    createCell(x: number, y: number) {
        const node = cc.instantiate(this.eCell);
        node.parent = this.eLayout;
        this.mCells[`${x}_${y}`] = node.getComponent(MapCell);
        this.mCells[`${x}_${y}`].init(x, y);
    }

    // 设置cell类型
    setCell(x: number, y: number, type: ECellType) {
        this.mCells[`${x}_${y}`].setType(type);
    }

    setCellVec(pos: cc.Vec2, type: ECellType) {
        this.mCells[`${pos.x}_${pos.y}`].setType(type);
    }

    updateStart(cell: cc.Vec2) {
        this.setCell(this.mStart.x, this.mStart.y, ECellType.PATH);
        this.setCell(cell.x, cell.y, ECellType.START);
        this.mStart = cell;
    }

    updateEnd(cell: cc.Vec2) {
        this.setCell(this.mEnd.x, this.mEnd.y, ECellType.PATH);
        this.setCell(cell.x, cell.y, ECellType.END);
        this.mEnd = cell;
    }
}
