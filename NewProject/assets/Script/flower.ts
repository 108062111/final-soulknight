const {ccclass, property} = cc._decorator;

@ccclass
export default class flower extends cc.Component {
    @property({type:cc.AudioClip})
    attackSE: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    deadSE: cc.AudioClip = null;

    @property(cc.Prefab)
    coinPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    energyPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;

    hp: number = 15;

    attackDist: number = 200;

    coin: number = 3;

    remainTime: number = 0;

    anim: cc.Animation = null;

    seeTarget: boolean = false;

    player: cc.Node = null;

    map: cc.Node = null;

    fail: cc.Node = null;

    attackCD: number = 3;

    attacktiming: number = 0;

    bulletNum: number = 12;

    iceCD: number = 2;

    isIce: boolean = false;

    isDead: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.anim = this.getComponent(cc.Animation);
        this.player = cc.find("Canvas/knight1");
        this.map = cc.find("Canvas/map");
        this.fail = cc.find("Canvas/Main Camera/fail");
    }

    start () {

    }

    update (dt) {
        if(this.fail.active || this.isDead) return;
        this.remainTime += dt;
        if(this.isIce) return;
        this.RaycastDetection(this.attackDist);
        if(this.seeTarget){
            // console.log("see");
            if (this.remainTime - this.attacktiming > this.attackCD){
                this.attacktiming = this.remainTime;
                cc.audioEngine.playEffect(this.attackSE,false);
                for(let i = 0; i < this.bulletNum; i++){
                    // console.log("new bullet");
                    let newBullet = cc.instantiate(this.bulletPrefab);
                    let pos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
                    pos = this.node.convertToNodeSpaceAR(cc.v2(pos.x, pos.y));
                    newBullet.position = pos;
                    newBullet.parent = this.node;
                    newBullet.getComponent('flower_bullet').init(i, this.bulletNum);
                }
            }
        }
    }

    onBeginContact(contact, self, other) {
        /* TODO: 
        1. bullet's tag
        2. - bullet's power
        */
        if(other.tag == 5) { // player bullet
            this.BeAttack(1);
        }
        else if(other.tag == 6) { // player rainbow
            this.BeAttack(0.02);
        }
        else if(other.tag == 7) { // player(wizard) small ice
            this.BeAttack(1);
            this.isIce = true;
            this.anim.stop();
            // console.log("ice");
            this.scheduleOnce(function() {
                this.isIce = false;
                this.anim.play("flower_idle");
            }, this.iceCD);
        }
    }

    BeAttack (data: number){
        this.hp -= data;
        if (!this.isDead && this.hp <= 0){
            this.isDead = true;
            this.node.parent.getComponent("small_room").dead_num += 1;
            this.anim.play("flower_dead");
            cc.audioEngine.playEffect(this.deadSE,false);
            this.scheduleOnce(function() {
                for (let i = 0; i < this.coin; i++){
                    let pos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
                    pos = this.map.convertToNodeSpaceAR(cc.v2(pos.x+Math.random()*60-30, pos.y+Math.random()*60-30));
                    
                    let coin = cc.instantiate(this.coinPrefab);
                    coin.position = pos;
                    coin.parent = this.map;
                }
                for (let i = 0; i < this.coin; i++){
                    let pos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
                    pos = this.map.convertToNodeSpaceAR(cc.v2(pos.x+Math.random()*60-30, pos.y+Math.random()*60-30));
                    
                    let energy = cc.instantiate(this.energyPrefab);
                    energy.position = pos;
                    energy.parent = this.map;
                }
                this.node.destroy();
            }, 0.5);
        }
    }

    RaycastDetection(dist: number){
        let results=cc.director.getPhysicsManager().rayCast(this.node.convertToWorldSpaceAR(cc.v2(0,0)),this.player.convertToWorldSpaceAR(cc.v2(0,0)),cc.RayCastType.All);  
        let playdis = 200000000, mapdis = 200000000;
        for(let j=0;j<results.length;j++){
            if(results[j].collider.tag == 0){
                if(mapdis > results[j].fraction)
                    mapdis = results[j].fraction;
            }
            else if(results[j].collider.tag == 1){
                playdis = results[j].fraction;
            }
        }
        if(playdis != 200000000 && playdis < mapdis){
            if(this.dist(this.node.convertToWorldSpaceAR(cc.v2(0,0)), this.player.convertToWorldSpaceAR(cc.v2(0,0))) < dist) this.seeTarget = true;
            else this.seeTarget = false;    
        }
        else this.seeTarget = false;    
        
        // hit = Physics2D.Raycast(transform.position + Vector3.up, (targetPosition.position - (transform.position + Vector3.up)).normalized, trackingRange, layerMask);
        // if (hit.transform != null && hit.transform == targetPosition){
        //     seeTarget = true;
        //     Debug.DrawLine(transform.position + Vector3.up, hit.transform.position, Color.red);
        // }
        // else{ seeTarget = false; }
    }

    dist(a: cc.Vec2,  b: cc.Vec2){
        let dx = a.x - b.x;
        let dy = a.y - b.y;
        return Math.sqrt(dx*dx+dy*dy);
    }
}
