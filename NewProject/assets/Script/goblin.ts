import {monster} from "./monster";

const {ccclass, property} = cc._decorator;

enum MonsterState {
    Idle,
    Stroll,
    Tracking,
    Attack,
    Die
}

@ccclass
export default class goblin extends monster {

    hp: number = 10;

    speed: number = 100;

    myClass: string = "goblin";

    @property(cc.Prefab)
    bullet:cc.Prefab = null;
    
    @property(cc.Node)
    gun:cc.Node = null;
    @property(cc.Node)
    contact:cc.Node = null;

    g:cc.Graphics=null;
    spawn()
    { 
        var mousePosition = this.player.convertToWorldSpaceAR(cc.v2(0,0)).sub(this.node.convertToWorldSpaceAR(cc.v2(0,0)));
        //this.shooting(mousePosition.x,mousePosition.y);

        if(mousePosition.x > 3){ // move to right
            if(this.node.scaleX < 0){
                this.node.scaleX *= -1;
            }
        }
        else if(mousePosition.x < -3){ // move to left
            if(this.node.scaleX > 0){
                this.node.scaleX *= -1;
            }
        }

        var newBullet = cc.instantiate(this.bullet);
        var x = mousePosition.x/cc.v2(mousePosition.x,mousePosition.y).mag();
        var y = mousePosition.y/cc.v2(mousePosition.x,mousePosition.y).mag();
        newBullet.setPosition(cc.v2(50,0));
        newBullet.parent = this.node.getChildByName('player_gun1');

        newBullet.getComponent('goblin_bullet').init(x, y,0);
    }
    shooting(posx,posy){
        var newBullet = cc.instantiate(this.bullet);
        newBullet.parent = this.node;
        var x = (posx-this.node.position.x)/cc.v2((posx-this.node.position.x),(posy-this.node.position.y)).mag();
        var y = (posy-this.node.position.y)/cc.v2((posx-this.node.position.x),(posy-newBullet.y)).mag();
        let angle=Math.atan(y/x)*180/Math.PI;
        if(x < 0){
            angle += 180;
        }
        var adding;
        if(this.node.scaleX<0 ){
            angle=-1*(angle-180);
            adding = this.node.position.add(cc.v2(0,-13));
            if(y>0){
                adding = adding.add(cc.v2(-40+y*6,0).rotate(-1*angle*0.0174533));
            }
            else{
                adding = adding.add(cc.v2(-43,0).rotate(-1*angle*0.0174533));
            }
        }
        else {
            adding = this.node.position.add(cc.v2(0,-10));
            if(y>0){
                adding = adding.add(cc.v2(40-y*10,0).rotate(angle*0.0174533));
            }
            else{
                adding = adding.add(cc.v2(43,0).rotate(angle*0.0174533));
            }

        }
        // this.gun.angle = angle;
        newBullet.setPosition(adding.x,adding.y);
        newBullet.getComponent('goblin_bullet').init(x, y);
    }
    
    autoshooting2(){
        this.gun.getComponent(cc.Animation).play("goblin_gun_fire");
            this.scheduleOnce(function() {
                    this.spawn();
            }, 0.2);

    }
    positionXY(){
        var mousePosition = this.player.convertToWorldSpaceAR(cc.v2(0,0)).sub(this.node.convertToWorldSpaceAR(cc.v2(0,0)));
        let angle = Math.atan(mousePosition.y/mousePosition.x)*180/Math.PI;
        if(mousePosition.x < 0 ){
            angle += 180;
        }
        if(this.node.scaleX<0 && this.gun.scaleX>0){
            angle=-1*(angle-180);
        }
        this.gun.angle = angle;
    }

    onLoad () {
        super.onLoad();
    }

    Attack () {
        // console.log("goblin attack");
        this.RaycastDetection(this.attackDist);
        if(!this.seeTarget){
            this.monsterState = MonsterState.Tracking;
            return;
        }
        this.positionXY();
        if (this.remainTime - this.attacktiming > this.attackCD){
            this.attacktiming = this.remainTime;
            // console.log("goblin shoot at ", this.node.position);
            this.anim.play(this.myClass + "_idle");
            cc.audioEngine.playEffect(this.attackSE,false);
            this.autoshooting2();
        }
    }

    // update (dt) {}
    

}
