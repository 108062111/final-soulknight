const {ccclass, property} = cc._decorator;

@ccclass
export default class weapon extends cc.Component {
    playerState: string = "0";

    currentWeapon: number[] = [1, 0, 0];

    weapon1: any;

    weapon1Number: any;

    weapon2: any;

    weapon2Number: any;

    weapon3: any;

    weapon3Number: any;

    weapon4: any;

    weapon4Number: any;

    @property(cc.Prefab)
    weapon1Prefab: cc.Prefab = null; // gun(knight)

    @property(cc.Prefab)
    weapon1NumberPrefab: cc.Prefab = null; // 0

    @property(cc.Prefab)
    weapon2Prefab: cc.Prefab = null; // rainbow

    @property(cc.Prefab)
    weapon2NumberPrefab: cc.Prefab = null; // 1

    @property(cc.Prefab)
    weapon3Prefab: cc.Prefab = null; // wand

    @property(cc.Prefab)
    weapon3NumberPrefab: cc.Prefab = null; // 5

    @property(cc.Prefab)
    weapon4Prefab: cc.Prefab = null; // book(wizard)

    @property(cc.Prefab)
    weapon4NumberPrefab: cc.Prefab = null; // 1


    onLoad () {
        this.node.on(cc.Node.EventType.MOUSE_DOWN, function(event){
            this.swapWeapon();
            event.stopPropagation()
        }, this);

        this.playerState = cc.find("Canvas/Main Camera/fail/playerstate").getComponent(cc.Label).string;
        this.playerState = cc.director.getScene().getChildByName('global').getComponent('persistnode').player;
        if(this.playerState == "0") { // knight
            this.currentWeapon = [1,0,0];
            this.initWeapon1();
        }
        else { // wizard
            this.currentWeapon = [4,0,0];
            this.initWeapon4();
        }
        this.initWeapon2();
        this.initWeapon3();
        //this.changeWeapon();
    }

    initWeapon1() {
        this.weapon1 = cc.instantiate(this.weapon1Prefab);
        this.weapon1.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon1.setPosition(-10, 0);
        this.weapon1.scaleX = 1;
        this.weapon1.scaleY = 1;

        this.weapon1Number = cc.instantiate(this.weapon1NumberPrefab);
        this.weapon1Number.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon1Number.setPosition(-35, 28);
    }

    initWeapon2() {
        this.weapon2 = cc.instantiate(this.weapon2Prefab);
        this.weapon2.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon2.setPosition(-10, -3);
        this.weapon2.active = false;

        this.weapon2Number = cc.instantiate(this.weapon2NumberPrefab);
        this.weapon2Number.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon2Number.setPosition(-35, 28);
        this.weapon2Number.active = false;
    }

    initWeapon3() {
        this.weapon3 = cc.instantiate(this.weapon3Prefab);
        this.weapon3.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon3.setPosition(5, 0);
        this.weapon3.active = false;

        this.weapon3Number = cc.instantiate(this.weapon3NumberPrefab);
        this.weapon3Number.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon3Number.setPosition(-35, 28);
        this.weapon3Number.active = false;
    }

    initWeapon4() {
        this.weapon4 = cc.instantiate(this.weapon4Prefab);
        this.weapon4.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon4.setPosition(7, 0);

        this.weapon4Number = cc.instantiate(this.weapon4NumberPrefab);
        this.weapon4Number.parent = cc.find("Canvas/Main Camera/weapon_bg");
        this.weapon4Number.setPosition(-35, 28);
    }

    swapWeapon() {
        if(this.currentWeapon[1] != 0) {
            if(this.currentWeapon[2] == 0) { // two weapons
                let tmp = this.currentWeapon[0];
                this.currentWeapon[0] = this.currentWeapon[1];
                this.currentWeapon[1] = tmp;

                this.changeWeapon();
            }
            else { // three weapons
                let tmp = this.currentWeapon[0];
                this.currentWeapon[0] = this.currentWeapon[1];
                this.currentWeapon[1] = this.currentWeapon[2];
                this.currentWeapon[2] = tmp;

                this.changeWeapon();
            }
        }
    }

    changeWeapon() {
        if(this.playerState == "0") { // knight
            this.weapon1.active = (this.currentWeapon[0] == 1);
            this.weapon1Number.active = (this.currentWeapon[0] == 1);
            cc.find("Canvas/knight1/player_gun1").active = (this.currentWeapon[0] == 1);
        }
        else { // wizard
            this.weapon4.active = (this.currentWeapon[0] == 4);
            this.weapon4Number.active = (this.currentWeapon[0] == 4);
            cc.find("Canvas/knight1/wizard_book").active = (this.currentWeapon[0] == 4);
        }
        
        this.weapon2.active = (this.currentWeapon[0] == 2);
        this.weapon2Number.active = (this.currentWeapon[0] == 2);
        cc.find("Canvas/knight1/player_rainbow_horse").active = (this.currentWeapon[0] == 2);
        
        this.weapon3.active = (this.currentWeapon[0] == 3);
        this.weapon3Number.active = (this.currentWeapon[0] == 3); 
        cc.find("Canvas/knight1/magic_wand").active = (this.currentWeapon[0] == 3);
        //cc.find("Canvas/magic_ball").active = (this.currentWeapon[0] == 3);
    }

    addWeapon(w: number) {
        if(this.currentWeapon[1] == 0) { // one weapon
            this.currentWeapon[1] = this.currentWeapon[0];
            this.currentWeapon[0] = w;

            this.changeWeapon();
        }
        else if(this.currentWeapon[2] == 0) { // two weapons
            this.currentWeapon[2] = this.currentWeapon[1];
            this.currentWeapon[1] = this.currentWeapon[0];
            this.currentWeapon[0] = w;

            this.changeWeapon();
        }
    }
}
