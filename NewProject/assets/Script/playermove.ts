const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component 
{
    //用來判別小地圖的
    before_room: string = "0";


    @property({type:cc.Node})
    camera: cc.Node = null;

    @property({type:cc.Node})
    status: cc.Node = null;



    @property({type:cc.AudioClip})//法師被攻擊: wizard_hurt
    wizard_hurt: cc.AudioClip = null;

    @property({type:cc.AudioClip})//一進map重生: reborn
    reborn: cc.AudioClip = null;

    @property({type:cc.AudioClip})//喝mp、hp瓶: drink_energy_bottle
    drink_energy_bottle: cc.AudioClip = null;


    @property({type:cc.AudioClip})//騎士被攻擊: knight_hurt
    knight_hurt: cc.AudioClip = null;

    @property({type:cc.AudioClip})//吃到金幣: get_coin
    get_coin: cc.AudioClip = null;
 

    stop:boolean = false;
    HP: number = 5;
    MP: number = 180;
    ARMOUR:number = 5;
    hp: cc.Label = null;
    mp: cc.Label = null;
    armour:cc.Label = null;
    maxhp:number = 5;
    maxmp:number = 180;
    maxarmour:number = 5;
    fulllength:number = 0;
    player:number = 0;
    @property
    frozen:boolean = false;
    frozenTime:number = 0;
    // LIFE-CYCLE CALLBACKS:
    @property
    posX:number = 0;
    posY:number = 0;

  

    @property({type:cc.Animation})
    animator: cc.Animation = null;

   
    private animstate: cc.AnimationState=null;

    public rebornPos : cc.Vec2 =cc.v2(0,0);

    private playerSpeedx: number = 0;

    private playerSpeedy: number = 0;

    private aDown: boolean = false; // key for player to go left

    private dDown: boolean = false; // key for player to go right

    private wDown: boolean = false; // key for player to shoot

    private sDown: boolean = false; // key for player to jump

    private isDead: boolean = false;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;      
        this.player=cc.director.getScene().getChildByName('global').getComponent('persistnode').player;
        //this.player = 0;
        this.node.getChildByName('wizard_book').active = false;
        this.node.getChildByName('player_gun1').active = false;
        if(this.player){
            this.getComponent(cc.Animation).play('wizard_reborn');
            this.maxhp = 3;
            this.maxarmour = 6;
            this.maxmp = 220;
        }  

        else{
            this.getComponent(cc.Animation).play('knight_reborn');
            this.maxhp = 5;
            this.maxarmour = 5;
            this.maxmp = 180;
        } 
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.HP = this.maxhp;
        this.MP = this.maxmp;
        this.ARMOUR = this.maxarmour;
        this.hp =  this.status.getChildByName('hp').getComponent(cc.Label);
        this.hp.string = ''+this.maxhp+'/'+this.maxhp;
        this.mp =  this.status.getChildByName('mp').getComponent(cc.Label);
        this.mp.string = ''+this.maxmp+'/'+this.maxmp;
        this.armour =  this.status.getChildByName('armour').getComponent(cc.Label);
        this.armour.string = ''+this.maxarmour+'/'+this.maxarmour;
        this.fulllength = this.status.getChildByName('hpbar').width;
    }

    onKeyDown(event) {
        this.playerSpeedx=0;
        this.playerSpeedy=0;
        if(!this.isDead){
            if(event.keyCode == cc.macro.KEY.a) {
                this.aDown=true;
            } else if( event.keyCode==cc.macro.KEY.d) {
                this.dDown=true;
            } 
            if(event.keyCode == cc.macro.KEY.w) {
                this.wDown=true;
            } 
            if(event.keyCode == cc.macro.KEY.s) {
                this.sDown = true;
            }
        }
    }
    onKeyUp(event) {
        if(event.keyCode==cc.macro.KEY.a) {
            this.aDown=false;
        } else if(event.keyCode==cc.macro.KEY.d) {
            this.dDown=false;
        } 
        else if(event.keyCode==cc.macro.KEY.w) {
            this.wDown=false;
         } 
         else if(event.keyCode==cc.macro.KEY.s) {
            this.sDown = false;
         } 
    }
    
    private playerMovement(dt) {
        if(!this.isDead && !this.stop){
            if(this.aDown==true && (this.wDown==true || this.sDown)) {
                this.playerSpeedx=-200*0.70710678118;
                //if(this.node.scaleX > 0)this.node.scaleX = -1*this.node.scaleX ;
            } else if(this.aDown==true) {
                this.playerSpeedx=-200;
                //if(this.node.scaleX > 0)this.node.scaleX = -1*this.node.scaleX ;
            } else if(this.dDown==true && (this.wDown==true || this.sDown)) {
                this.playerSpeedx=200*0.70710678118;
                //if(this.node.scaleX > 0)this.node.scaleX = -1*this.node.scaleX ;
            } else if(this.dDown==true) {
                this.playerSpeedx=200;
                //if(this.node.scaleX < 0)this.node.scaleX = -1*this.node.scaleX ;
           
            }else{
                this.playerSpeedx=0;
            }
            if(this.wDown && (this.dDown==true || this.aDown)) {
                this.playerSpeedy=200*0.70710678118;
            }else if(this.wDown ) {
                this.playerSpeedy=200;
            }else if(this.sDown && (this.dDown==true || this.aDown)) {
                this.playerSpeedy=-200*0.70710678118;
            }else if(this.sDown) {
                this.playerSpeedy=-200;
            } else{
                this.playerSpeedy=0;
            }
            this.node.x += this.playerSpeedx * dt;
            this.node.y += this.playerSpeedy * dt;
        }
    }    

    start () {
        cc.director.getCollisionManager().enabled = true;
        this.schedule(function() {
            cc.audioEngine.playEffect(this.reborn, false);
            if(this.player){
                this.getComponent(cc.Animation).play('wizard_reborn');
            }
            else this.getComponent(cc.Animation).play('knight_reborn');
        },0,1, 0);
        this.schedule(function() {
            cc.find("Canvas/Main Camera/weapon_bg").getComponent("weapon").changeWeapon();
        },0,1, 0.5);
        this.schedule(function() {
            if(this.ARMOUR < this.maxarmour && !this.isDead){
                this.ARMOUR += 1;
                this.armour.string = ''+this.ARMOUR+'/'+this.maxarmour;
                this.status.getChildByName('armourbar').width = this.ARMOUR*this.fulllength/this.maxarmour;
                this.status.getChildByName('armourbar').x += (this.fulllength/this.maxarmour)/2;
         
            }
        }, 4);
    }
    drinklife(){
        cc.audioEngine.playEffect(this.drink_energy_bottle, false);
        if(this.HP < this.maxhp && !this.isDead){
            var diff = this.maxhp - this.HP;
            this.HP = this.maxhp;
            this.hp.string = ''+this.HP+'/'+this.maxhp;
            this.status.getChildByName('hpbar').width = this.HP*this.fulllength/this.maxhp;
            this.status.getChildByName('hpbar').x += (diff*this.fulllength/this.maxhp)/2;
     
        }
    }
    drinkenergy(){
        cc.audioEngine.playEffect(this.drink_energy_bottle, false);
        if(this.MP < this.maxmp && !this.isDead){
            var diff = this.maxmp - this.MP;
            this.MP = this.maxmp;
            this.mp.string = ''+this.MP+'/'+this.maxmp;
            this.status.getChildByName('mpbar').width = this.MP*this.fulllength/this.maxmp;
            this.status.getChildByName('mpbar').x += (diff*this.fulllength/this.maxmp)/2;
     
        }
    }
    positionXY(event)
    {
        var playerPosition = cc.v2(this.node.position.x,this.node.position.y);
        var mousePosition = event.getLocation();

        mousePosition = this.node.parent.convertToNodeSpaceAR(mousePosition);
        var angle = mousePosition.signAngle(playerPosition);
        var angleD = cc.misc.radiansToDegrees(angle);
        angleD = (angleD * -1)-45;
        this.node.angle = angleD;
    }

    loadGameScene(){
        this.isDead=false;
    }
   
  
    
    update(dt) {
        if(!this.frozen){
            this.playerMovement(dt);
            this.PlayerAnimation();
        }
        this.camerafollow();
        cc.find("Canvas/magicball").x = this.node.x + 7.224;
        cc.find("Canvas/magicball").y = this.node.y + 7.224;
        if(this.frozen) {
            this.frozenTime += dt;
            if(this.frozenTime >= 2) {
                this.frozenTime = 0;
                this.frozen = false;
            }
        }
    }

   
    onDestroy () { 
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    camerafollow(){
        this.camera.x = this.node.x;
        this.camera.y = this.node.y;
    }
    
   
    PlayerAnimation(){
        var anim = this.getComponent(cc.Animation);
        if(this.isDead && (this.animstate.name != 'knight_dead' && this.animstate.name != 'wizard_dead')){
            var finishAction = cc.callFunc(() => {
                anim.pause();
                if(!this.player)this.animstate = anim.play('knight_dead');
                else this.animstate = anim.play('wizard_dead');
            });
            this.node.runAction(finishAction);
            
        }else if(this.isDead==false){
            if(this.playerSpeedx  != 0 || this.playerSpeedy  != 0){
                if(this.animstate == null || this.animstate.name == 'knight_idle' || this.animstate.name == 'wizard_idle'){
                    anim.pause();
                    if(!this.player)this.animstate = anim.play('knight_walk');
                    else this.animstate = anim.play('wizard_walk');
                }
            }
            else{
               if(this.animstate == null || (this.animstate.name != 'knight_idle' && this.animstate.name!='wizard_idle')){
                anim.pause();
                if(!this.player)this.animstate = anim.play('knight_idle');
                else this.animstate = anim.play('wizard_idle');
               }
                
            }
        }
    }
   

    onBeginContact(contact, self, other) {
        if(other.tag==2){
            contact.enabled = false;
        }
        else if(other.tag == 13){
            this.harm(1);
        }
        else if(other.tag == 14){
            this.harm(1);
        }
        else if(other.tag == 15){
            this.harm(1);
            this.frozen = true;
        }
        else if(other.tag == 16){
            this.harm(2);
        }
        else if(other.tag == 18){
            this.harm(1);
        }
        else if(other.tag == 19){
            this.harm(1);
        }
    }
    harm(harm:number){
        var harmleft = harm;
        if(this.player)cc.audioEngine.playEffect(this.wizard_hurt, false);
        else cc.audioEngine.playEffect(this.knight_hurt, false);
        if(this.ARMOUR > 0){
            if(this.ARMOUR >= harm){
                this.ARMOUR -= harm;
                harmleft = 0;
            }
            else{
                harmleft -= this.ARMOUR;
                harm = this.ARMOUR;
                this.ARMOUR = 0;
            }
            this.armour.string = ''+this.ARMOUR+'/'+this.maxarmour;
            this.status.getChildByName('armourbar').width = this.ARMOUR*this.fulllength/this.maxarmour;
            this.status.getChildByName('armourbar').x -= ((harm)*this.fulllength/this.maxarmour)/2;
        }
        if(this.HP > 0 && harmleft > 0){
            if(this.HP >= harmleft){
                this.HP -= harmleft;
            }
            else{
                harmleft = this.HP;
                this.HP = 0;
            }
            this.hp.string = ''+this.HP+'/'+this.maxhp;
            this.status.getChildByName('hpbar').width = this.HP*this.fulllength/this.maxhp;
            this.status.getChildByName('hpbar').x -= ((harmleft)*this.fulllength/this.maxhp)/2;
        }
        if(this.HP == 0){
            this.isDead = true;
            cc.director.getPhysicsManager().enabled = false;   
            this.node.active = false;

        }
    }
    usemp(usedmp:number){
        if(this.MP >= usedmp){
            this.MP -= usedmp;
            this.mp.string = ''+this.MP+'/'+this.maxmp;
            this.status.getChildByName('mpbar').width = this.MP*this.fulllength/this.maxmp;
            this.status.getChildByName('mpbar').x -= ((usedmp)*this.fulllength/this.maxmp)/2;
            return 1;
        }
        else{
            this.mp.string = '0/'+this.maxmp;
            this.status.getChildByName('mpbar').width = 0;
            this.status.getChildByName('mpbar').x -= ((this.MP)*this.fulllength/this.maxmp)/2;
            this.MP = 0;
            return 0;

        }
    }
    addmp(n: number){
        if(this.MP < this.maxmp && !this.isDead){
            var diff = this.maxmp - this.MP;
            diff = Math.min(diff,n);
            this.MP += diff;
            this.mp.string = ''+this.MP+'/'+this.maxmp;
            this.status.getChildByName('mpbar').width = this.MP*this.fulllength/this.maxmp;
            this.status.getChildByName('mpbar').x += (diff*this.fulllength/this.maxmp)/2;
        }
    }

}
