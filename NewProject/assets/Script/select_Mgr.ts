const {ccclass, property} = cc._decorator;

@ccclass
export default class select_Mgr extends cc.Component {
    
    @property({type:cc.AudioClip})
    buttoneffect: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    entereffect: cc.AudioClip = null;

    onLoad () {
        firebase.auth().onAuthStateChanged(function(user) {
            // Check user login
            if (user) {
                let Email: string = user.email.replace(/\./gi,"");
                //用Email撈firebase其他資料填上
                var finduser = firebase.database().ref('user').child(Email);
                finduser.once('value').then(function(snapshot) {
                    var childData = snapshot.val();
                    cc.find("Canvas/record/username").getComponent(cc.Label).string = childData.username;
                    //時間
                    let timer: number = childData.time;
                    let tmp1: number = timer/60;
                    let tmp2: number = timer/3600;
                    let sec:number = timer%60;
                    let min:number = Number(tmp1.toFixed(0))%60;
                    let hour:number = Number(tmp2.toFixed(0));
                    cc.log("timer = "+hour+": "+min+": "+sec);
                    cc.find("Canvas/record/hour").getComponent(cc.Label).string = hour.toString();
                    cc.find("Canvas/record/min").getComponent(cc.Label).string = min.toString();
                    cc.find("Canvas/record/sec").getComponent(cc.Label).string = sec.toString();
                    //日期
                    cc.find("Canvas/record/year").getComponent(cc.Label).string = childData.year.toString();
                    cc.find("Canvas/record/month").getComponent(cc.Label).string = childData.month.toString();
                    cc.find("Canvas/record/date").getComponent(cc.Label).string = childData.date.toString();
                    //決定這次是要用knight還是wizard的圖
                    if(childData.playerstate == 0){
                        cc.find("Canvas/record/knight").active = true;
                    }else if(childData.playerstate == 1){
                        cc.find("Canvas/record/wizard").active = true;
                    }
                    //看record可不可以開放
                    if(childData.record == 1){
                        cc.find("Canvas/recording_board2").active = true;
                        cc.find("Canvas/recording_board1").active = false;
                    }
                }).catch(e => console.log(e.message));
                //(結束)用Email撈其他資料填上
            }
        });
    }
    //按下knight就會開始遊戲
    Button_knight (event, customEventData) {
        cc.audioEngine.playEffect(this.entereffect, false);
        cc.log("select knight!");
        cc.director.getScene().getChildByName('global').getComponent('persistnode').player=0;
        
        firebase.auth().onAuthStateChanged(function(user) {
            // Check user login
            if (user) {
                let Email: string = user.email.replace(/\./gi,"");
                var finduser = firebase.database().ref('user').child(Email);
                //更新firebase
                var data = {
                    playerstate: 0
                };
                finduser.update(data);

                //todo 要換場景
                cc.director.loadScene("Map");
            }
        });
    }
    //按下wizard就會開始遊戲
    Button_wizard (event, customEventData) {
        cc.audioEngine.playEffect(this.entereffect, false);
        cc.log("select wizard!");
        cc.director.getScene().getChildByName('global').getComponent('persistnode').player=1;
        firebase.auth().onAuthStateChanged(function(user) {
            // Check user login
            if (user) {
                let Email: string = user.email.replace(/\./gi,"");
                var finduser = firebase.database().ref('user').child(Email);
                //更新firebase
                var data = {
                    playerstate: 1
                };
                finduser.update(data);

                //todo 要換場景
                cc.director.loadScene("Map");
            }
        });
    }
    //按下record2就會打開record
    Button_record (event, customEventData) {
        cc.audioEngine.playEffect(this.buttoneffect, false);
        cc.log("open record!");
        cc.find("Canvas/record").active = true;
        cc.find("Canvas/knight1").active = false;
        cc.find("Canvas/wizard1").active = false;
        cc.find("Canvas/recording_board2").active = false;
    }
    //record的close
    Button_close (event, customEventData) {
        cc.audioEngine.playEffect(this.buttoneffect, false);
        cc.log("press record's close");
        cc.find("Canvas/record").active = false;
        cc.find("Canvas/knight1").active = true;
        cc.find("Canvas/wizard1").active = true;
        cc.find("Canvas/recording_board2").active = true;
    }
}
