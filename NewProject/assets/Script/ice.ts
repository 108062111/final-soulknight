const {ccclass, property} = cc._decorator;

@ccclass
export default class ice extends cc.Component {
    anim = null;

    isdead: boolean = false;

    interval: number = 0;

    @property({type:cc.AudioClip})
    iceDisappearSound: cc.AudioClip = null;

    playEffect(sound){
        cc.audioEngine.playEffect(sound, false);
    }

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    update(dt) {
        if(cc.find("Canvas/Main Camera/fail").active) {
            this.node.destroy();
        }

        this.interval += dt;
        if(!this.isdead && this.interval > 2) {
            this.isdead = true;
            this.playEffect(this.iceDisappearSound);
            this.anim.play("ice_dead");
            let action = cc.fadeOut(2);
            this.node.runAction(action);
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 2);
        }
    }
}
