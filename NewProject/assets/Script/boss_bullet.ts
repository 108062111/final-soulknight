const {ccclass, property} = cc._decorator;

@ccclass
export default class boss_bullet extends cc.Component {
    anim = null;

    num: number;

    speed: number = 200;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    public init(node: cc.Node, n: number) 
    {
        this.num = n;
        this.setInitPos(node);
        this.bulletMove();
    }

    private setInitPos(node: cc.Node)
    {
        this.node.parent = node.parent;

        let dx = (cc.find("Canvas/knight1").x - cc.find("Canvas/map").x)/cc.find("Canvas/map").scaleX - node.x;
        let dy = (cc.find("Canvas/knight1").y - cc.find("Canvas/map").y)/cc.find("Canvas/map").scaleY - node.y;
        let angle = Math.atan(dy/dx)*180/Math.PI;
        if(dx < 0)
            angle += 180;

        if(this.num == 0) {
            this.node.position = cc.v2(node.position.x, node.position.y);
            this.node.angle = angle;
        }
        else if(this.num == 1) {
            this.node.position = cc.v2(node.position.x, node.position.y);
            this.node.angle = angle+30;
        }
        else if(this.num == 2) {
            this.node.position = cc.v2(node.position.x, node.position.y);
            this.node.angle = angle-30;
        }
    }

    private bulletMove() {
        let x = Math.cos(this.node.angle*Math.PI/180)*this.speed;
        let y = Math.sin(this.node.angle*Math.PI/180)*this.speed;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(x, y);
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 0) { // wall
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);

            this.anim.play("boss_bullet_dead");
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
        else if(other.tag == 1) { // player
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);
            this.node.destroy();
        }
    }

}
