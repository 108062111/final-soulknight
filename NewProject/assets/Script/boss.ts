const {ccclass, property} = cc._decorator;

@ccclass
export default class boss extends cc.Component {
    anim = null;

    hand = null;

    HP: number = 100;

    maxHP: number = 100;

    frozen: boolean = false;

    frozenTime: number = 0;

    enter: boolean = false;

    isdead: boolean = false;

    /* bullet, small ball, big ball, ice thorn */
    probability: number[] = [0.2, 0.2, 0.2, 0.2];

    /* -1: dead, 0: idle, 1~3: bullet, 4~39: small ball, 40: big ball, 41~45: ice thorn */
    bossState: number = 0;

    interval: number = 0; // compute time

    rainbowAttack: number = 0.01;

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    private smallBallPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    private bigBallPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    private iceThornPrefab: cc.Prefab = null;

    @property({type:cc.AudioClip})
    bulletSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    smallBallSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    bigBallSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    iceThornSound: cc.AudioClip = null;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
        this.hand =  this.node.getChildByName("boss_hand");
    }

    update(dt) {
        cc.find("Canvas/Main Camera/boss_hp/red").width = this.HP/this.maxHP*300;
        
        if(this.isdead || cc.find("Canvas/Main Camera/fail").active || !this.enter)
            return;
        
        if(this.frozen) {
            this.bossState = 0;
            this.frozenTime += dt;
            if(this.frozenTime >= 2) {
                this.frozenTime = 0;
                this.frozen = false;
            }
            return;
        }

        if(this.bossState == -1) {
            this.isdead = true;
            this.anim.play("boss_dead");
            this.hand.destroy();
        }
        if(this.bossState == 0) { // idle
            this.hand.angle = 0;
            this.hand.getComponent(cc.Animation).play("boss_hand_idle");
            this.interval += dt;

            if(this.interval >= 0.8) {
                this.interval = 0;
    
                if(Math.random() < this.probability[0]) { // bullet
                    this.bossState = 1;
                    this.playEffect(this.bulletSound);
                }
                else if(Math.random() < this.probability[0]+this.probability[1]) { // small ball
                    this.bossState = 4;
                    this.playEffect(this.smallBallSound);
                }
                else if(Math.random() < this.probability[0]+this.probability[1]+this.probability[2]) { // big ball
                    this.bossState = 40;
                    this.playEffect(this.bigBallSound);
                }
                else if(Math.random() < this.probability[0]+this.probability[1]+this.probability[2]+this.probability[3]) { // ice thorn
                    this.bossState = 41;
                    this.playEffect(this.iceThornSound);
                }
            }
        }
        else if(this.bossState >= 1 && this.bossState <= 3) { // bullet(middle, up, down)
            this.createBullet(this.bossState-1);
            this.bossState = (this.bossState == 3) ? 0 : (this.bossState + 1);
        }
        else if(this.bossState >= 4 && this.bossState <= 39) { // small ball
            this.hand.angle += 10;
            this.hand.getComponent(cc.Animation).play("boss_hand_rotation");

            this.interval += dt;
            if(this.interval >= 0.15) {
                this.interval = 0;

                this.createSmallBall(this.bossState-4);
                this.bossState = (this.bossState == 39) ? 0 : (this.bossState + 1);
            }
        }
        else if(this.bossState == 40) { // big ball
            this.createBigBall();
            this.bossState = 0;
        }
        else if(this.bossState >= 41 && this.bossState <= 45) { // ice thorn
            this.createIce(this.bossState-41, this.node.position, 0);
            this.bossState = (this.bossState == 45) ? 0 : (this.bossState + 1);
        }
    }

    playEffect(sound){
        cc.audioEngine.playEffect(sound, false);
    }

    private createBullet(n: number) {
        let bullet = cc.instantiate(this.bulletPrefab);
        bullet.getComponent('boss_bullet').init(this.node, n);

    }

    private createSmallBall(n: number) {
        let bullet = cc.instantiate(this.smallBallPrefab);
        bullet.getComponent('boss_small_ball').init(this.node, n);

    }

    private createBigBall() {
        let bullet = cc.instantiate(this.bigBallPrefab);
        bullet.getComponent('boss_big_ball').init(this.node);

    }

    private createIce(n: number, last_pos: cc.Vec2, i: number) {
        let bullet = cc.instantiate(this.iceThornPrefab);
        bullet.parent = this.node.parent;

        if(n == 0) {
            bullet.position = cc.v2(last_pos.x, last_pos.y+6);
        }
        else if(n == 1) {
            bullet.position = cc.v2(last_pos.x+10, last_pos.y+3);
        }
        else if(n == 2) {
            bullet.position = cc.v2(last_pos.x+6, last_pos.y-10);
        }
        else if(n == 3) {
            bullet.position = cc.v2(last_pos.x-6, last_pos.y-10);
        }
        else if(n == 4) {
            bullet.position = cc.v2(last_pos.x-10, last_pos.y+3);
        }

        this.scheduleOnce(function() {
            if(bullet.active && i <= 100)
                this.createIce(n, bullet.position, i+1);
        }, 0.05);
        
        this.scheduleOnce(function() {
            if(bullet.active)
                bullet.destroy();
        }, 1);
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 5) { // player's bullet
            this.HP -= 1;
            
            if(this.HP <= 0) { // dead
                this.HP = 0;
                if(this.bossState != -1) {
                    this.bossState = -1;
                    cc.find("Canvas/Main Camera/boss_hp").active = false; //hide boss hp bar
                    cc.find("Canvas/map/for_small_map/7").getComponent("small_room").dead_num = 1;
                }
            }
        }
        else if(other.tag == 6) { // rainbow
            this.HP -= this.rainbowAttack;
            
            if(this.HP <= 0) { // dead
                this.HP = 0;
                if(this.bossState != -1) {
                    this.bossState = -1;
                    cc.find("Canvas/Main Camera/boss_hp").active = false; //hide boss hp bar
                    cc.find("Canvas/map/for_small_map/7").getComponent("small_room").dead_num = 1;
                }
            }
        }
    }
}
