import {monster} from "./monster";

const {ccclass, property} = cc._decorator;

enum MonsterState {
    Idle,
    Stroll,
    Tracking,
    Attack,
    Die
}

@ccclass
export default class pig extends monster {

    hp: number = 10;

    trackDist: number = 400;

    attackDist: number = 300;

    strollCD: number = 0.4;

    attackCD: number = 5;

    attacktiming: number = 0;

    attackAnimCD: number = 0.5;

    // attackAnimtiming: number = 0;

    myClass: string = "pig";
    
    onLoad () {
        super.onLoad();
    }

    Attack () {
        // console.log("pig attack");
        if(this.remainTime - this.attacktiming < this.attackAnimCD)
            return;
        this.strollCD = 0.25;
        this.Tracking();
        this.RaycastDetection(this.attackDist);
        if(!this.seeTarget){
            // console.log("attack not see to stroll");
            this.strollCD = 0.4;
            this.monsterState = MonsterState.Stroll;
        }
    }

    onBeginContact(contact, self, other) {
        if(this.isIce) return;
        super.onBeginContact(contact, self, other);
        if(other.tag == 1) { // player
            if(this.monsterState == MonsterState.Attack){
                // console.log("attack and back to stroll");
                this.player.getComponent("playermove").harm(1);
                this.attacktiming = this.remainTime;

                var dif = this.player.convertToWorldSpaceAR(cc.v2(0,0)).sub(this.node.convertToWorldSpaceAR(cc.v2(0,0)));
                if(dif.x > 3){ // move to right
                    if(this.node.scaleX < 0){
                        this.node.scaleX *= -1;
                    }
                }
                else if(dif.x < -3){ // move to left
                    if(this.node.scaleX > 0){
                        this.node.scaleX *= -1;
                    }
                }        

                this.anim.play("pig_attack");
                cc.audioEngine.playEffect(this.attackSE,false);
                this.scheduleOnce(function() {
                    this.monsterState = MonsterState.Stroll;
                    this.strollCD = 0.4;
                    this.star.RandomPath();
                    let mEnd = this.star.getEnd();
                    this.star.setNewStartEnd(this.mPos, mEnd);
                    this.updateStart(this.mPos);
                    this.updateEnd(mEnd);
                }, this.attackAnimCD);
            }
        }
    }

    // update (dt) {}
    

}