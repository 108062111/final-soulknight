
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    speed:number=800;
    anim = null;
    // LIFE-CYCLE CALLBACKS:
    player:number = 0;
    
    // onLoad () {}

    start () {
        this.anim = this.getComponent(cc.Animation);
        this.player=cc.director.getScene().getChildByName('global').getComponent('persistnode').player;
        //this.player = 0;
    }
    init(x:number,y:number,angle:number=Math.atan(y/x)*180/Math.PI){
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(x*this.speed, y*this.speed);
        this.node.angle = angle;
       
    }
    update (dt) {
    }
    onBeginContact(contact, self, other) {
        if(other.tag==1){
            contact.enabled = false;
        }
        else if((other.tag==0)||(other.tag>=10 && other.tag<=13) ){
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            if(!this.player)this.anim.play("player_bullet_dead");
            else this.anim.play("book_bullet_dead");
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
    }
}

