const {ccclass, property} = cc._decorator;

@ccclass
export default class small_ice extends cc.Component {
    interval: number = 0;

    @property(cc.Prefab)
    private icePrefab: cc.Prefab = null;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.scheduleOnce(function() {
            this.node.destroy();
        }, 1);
    }

    update(dt) {
        this.interval += dt;
        if(this.interval >= 1) {
            this.interval = 1;
        }
        if(cc.find("Canvas/Main Camera/fail").active) {
            this.node.destroy();
        }
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 10) { // boss
            if(this.interval <= 0.1) {
                var worldPos = other.node.convertToWorldSpaceAR(cc.v2(0,0));
                var localPos = cc.find("Canvas").parent.convertToNodeSpaceAR(worldPos);
                let ice = cc.instantiate(this.icePrefab);
                ice.parent = cc.find("Canvas").parent;
                ice.scaleX = 3;
                ice.scaleY = 3;
                ice.setPosition(localPos);

                cc.find("Canvas/map/boss").getComponent("boss").frozen = true;
                cc.find("Canvas/map/boss").getComponent("boss").HP -= 3;
            }
        }
        else if(other.tag >= 11 && other.tag <= 13) { // emermy
            if(this.interval <= 0.1) {
                var worldPos = other.node.convertToWorldSpaceAR(cc.v2(0,0));
                var localPos = cc.find("Canvas").parent.convertToNodeSpaceAR(worldPos);
                let ice = cc.instantiate(this.icePrefab);
                ice.parent = cc.find("Canvas").parent;
                ice.setPosition(localPos);
            } 
        }
        else {
            contact.disabled = true;
        }
    }
}
