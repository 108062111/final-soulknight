const {ccclass, property} = cc._decorator;

@ccclass
export default class coin extends cc.Component {
    @property({type:cc.AudioClip})
    deadSE: cc.AudioClip = null;

    player: cc.Node = null;

    attractDist: number = 100;

    moveTiming: number = 0.5;

    isDead: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.player = cc.find("Canvas/knight1");
    }

    // start () {

    // }

    update (dt) {
        if(this.isDead) return;
        let coinAbsPos = this.node.convertToWorldSpaceAR(cc.v2(0,0));
        let playerAbsPos = this.player.convertToWorldSpaceAR(cc.v2(0,0));
        if(this.dist(coinAbsPos,playerAbsPos) < this.attractDist){
            this.isDead = true;
            let coin_num = cc.find("Canvas/Main Camera/coin/coin_num").getComponent(cc.Label);
            coin_num.string = (parseInt(coin_num.string) + 1).toString();
            let diff = playerAbsPos.sub(coinAbsPos)
            let action = cc.moveBy(this.moveTiming, diff.x/3, diff.y/3); 
            this.node.runAction(action);
            cc.audioEngine.playEffect(this.deadSE,false);
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
    }
    
    dist(a: cc.Vec2,  b: cc.Vec2){
        let dx = a.x - b.x;
        let dy = a.y - b.y;
        return Math.sqrt(dx*dx+dy*dy);
    }
}
