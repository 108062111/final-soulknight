
const {ccclass, property} = cc._decorator;

@ccclass
export default class playershoot extends cc.Component {
    @property(cc.Prefab)
    knight_bullet:cc.Prefab = null;
    @property(cc.Prefab)
    wizard_bullet:cc.Prefab = null;
    
    bullet:cc.Prefab = null;
    @property(cc.Node)
    gun:cc.Node = null;
    @property(cc.Node)
    gun2:cc.Node = null;
    @property(cc.Node)
    contact:cc.Node = null;


    @property({type:cc.AudioClip})//法杖攻擊: wand
    wand: cc.AudioClip = null;


    @property({type:cc.AudioClip})//法師的書攻擊: wizard_bullet
    wizardbullet: cc.AudioClip = null;


    @property({type:cc.AudioClip})//騎士一開始的槍攻擊: knight_fire
    knight_fire: cc.AudioClip = null;
 

    @property
    posX:number = 0;
    posY:number = 0;
    score:number = 0;
    auto:number=0;
    player:number = 0;
    g:cc.Graphics=null;
    swingnum:number = 0;
    rotating:boolean = false;
    spawn(event){ 
        var mousePosition;
        if(!this.player){
            mousePosition = event.getLocation().sub(cc.v2(this.node.width/2,this.node.height/2));
            var newBullet = cc.instantiate(this.bullet);
            var x = mousePosition.x/cc.v2(mousePosition.x,mousePosition.y).mag();
            var y = mousePosition.y/cc.v2(mousePosition.x,mousePosition.y).mag();
            newBullet.setPosition(cc.v2(50,0));
            newBullet.parent = this.gun;
            newBullet.getComponent('playerbullet1').init(x, y,0);
            if(this.gun2.active){
                mousePosition = event.getLocation().sub(cc.v2(this.node.width/2,this.node.height/2));
                var Bullet = cc.instantiate(this.bullet);
                var x = mousePosition.x/cc.v2(mousePosition.x,mousePosition.y).mag();
                var y = mousePosition.y/cc.v2(mousePosition.x,mousePosition.y).mag();
                Bullet.setPosition(cc.v2(50,0));
                Bullet.parent = this.gun2;
                Bullet.getComponent('playerbullet1').init(x, y,0);
            }
        }
        else{
            if(this.node.getChildByName('knight1').scaleX>0)mousePosition = event.getLocation().sub(cc.v2(this.node.width/2+10,this.node.height/2-2.8));
            else{
                mousePosition = event.getLocation().sub(cc.v2(this.node.width/2-10,this.node.height/2-2.8));
            }
            var newBullet = cc.instantiate(this.bullet);
            var x = mousePosition.x/cc.v2(mousePosition.x,mousePosition.y).mag();
            var y = mousePosition.y/cc.v2(mousePosition.x,mousePosition.y).mag();
            newBullet.setPosition(cc.v2(0,0));
            newBullet.parent = this.node.getChildByName('knight1').getChildByName('wizard_book');
            newBullet.getComponent('playerbullet1').init(x, y,0);
        }
    }

    shooting(posx,posy){
        var newBullet = cc.instantiate(this.bullet);
        var x;
        var y;
        if(this.player==0){
            var posx1 = posx- this.gun.convertToWorldSpaceAR(cc.v2(0,0)).x;
            var posy1 = posy- this.gun.convertToWorldSpaceAR(cc.v2(0,0)).y;
            x = (posx1)/cc.v2((posx1),(posy1)).mag();
            y = (posy1)/cc.v2((posx1),(posy1)).mag();
            let angle=Math.atan(y/x)*180/Math.PI;
            if(x < 0 ){
                angle = -angle;
                this.node.getChildByName('knight1').scaleX = -2.408;
            }
            else{
                this.node.getChildByName('knight1').scaleX = 2.408;
            }
            this.gun.angle = angle;
            newBullet.setPosition(cc.v2(50,0));
            newBullet.parent = this.node.getChildByName('knight1').getChildByName('player_gun1');
            newBullet.getComponent('playerbullet1').init(x, y,0);
            if(this.gun2.active){
                var Bullet = cc.instantiate(this.bullet);
                var posx2 = posx- this.gun2.convertToWorldSpaceAR(cc.v2(0,0)).x;
                var posy2 = posy- this.gun2.convertToWorldSpaceAR(cc.v2(0,0)).y;
                x = (posx2)/cc.v2((posx2),(posy2)).mag();
                y = (posy2)/cc.v2((posx2),(posy2)).mag();
                let angle=Math.atan(y/x)*180/Math.PI;
                if(x < 0 ){
                    angle = -angle;
                    this.node.getChildByName('knight1').scaleX = -2.408;
                }
                else{
                    this.node.getChildByName('knight1').scaleX = 2.408;
                }
                this.gun2.angle = angle;
                Bullet.setPosition(cc.v2(50,0));
                Bullet.parent = this.gun2;
                Bullet.getComponent('playerbullet1').init(x, y,0);
            }
            
        }
        else if(this.player == 1){
            newBullet.parent = this.node.getChildByName('knight1').getChildByName('wizard_book');
            var startx = this.node.getChildByName('knight1').getChildByName('wizard_book').position.x;
            var starty = this.node.getChildByName('knight1').getChildByName('wizard_book').position.y;
            posx = posx- this.node.getChildByName('knight1').getChildByName('wizard_book').convertToWorldSpaceAR(cc.v2(0,0)).x;
            posy = posy- this.node.getChildByName('knight1').getChildByName('wizard_book').convertToWorldSpaceAR(cc.v2(0,0)).y;
            x = (posx)/cc.v2((posx),(posy)).mag();
            y = (posy)/cc.v2((posx),(posy)).mag();
            let angle = Math.atan(y/x)*180/Math.PI;
            if(x < 0 ){
                angle = -angle;
                this.node.getChildByName('knight1').scaleX = -2.408;
            }
            else{
                this.node.getChildByName('knight1').scaleX = 2.408;
            }
            newBullet.setPosition(0,0);
            newBullet.getComponent('playerbullet1').init(x, y);
        }
        
    }
    magicball(event){
        var avail = this.node.getChildByName('knight1').getComponent("playermove").usemp(5);
        if(avail ){
            var wound = this.node.getChildByName('knight1').getChildByName('magic_wand');
            var magicball = cc.find("Canvas/magicball");
            var swing = cc.sequence(
                cc.rotateBy(0.2,60),
                cc.rotateBy(0.2,-60),
            );
            magicball.active = true;
            if(wound.rotation==0){
                cc.audioEngine.playEffect(this.wand, false);
                wound.runAction(swing);
                this.swingnum+=1;
            }
            this.scheduleOnce(function() {
                this.swingwound = false;
                this.rotating = true;
                magicball.runAction(cc.sequence(cc.rotateBy(0.5,360).repeat(16*this.swingnum),cc.rotateTo(0,0),finished));
            }, 0.4);
            var finished = cc.callFunc(function() {
                this.rotating = false;
                magicball.active = false;
                cc.find("Canvas/magicball/magic_ball pink").active = false;
            }, this);
            
        }
    }
    autoshooting2(event){ 
        var dis = 200000000;
        var target;
        var targetpos;
        var pos;
        var actual;
        if(this.player)actual=this.node.getChildByName('knight1').getChildByName('wizard_book');
        else actual = this.node.getChildByName('knight1').getChildByName('player_gun1');
        for(let i=360;i>=0;i-=2){  
            let v1= cc.v2(Math.cos(Math.PI * i / 180)*1000,Math.sin(Math.PI * i / 180)*1000);  
            let v2=actual.convertToWorldSpaceAR(v1);  
            let results=cc.director.getPhysicsManager().rayCast(actual.convertToWorldSpaceAR(cc.v2(0,0)) ,v2,cc.RayCastType.All);  
            var closest = 200000000;
            var thisdis = 200000000;
            var thistarget;
            var collide;
            for(let j=0;j<results.length;j++){  
                var thisenemy = results[j].collider.node;
                var thisenemypos=  thisenemy.convertToWorldSpaceAR(cc.v2(0,0));    
                var temp = thisenemypos.sub(actual.convertToWorldSpaceAR(cc.v2(0,0))).mag();
                if(results[j].collider.tag>=10 && results[j].collider.tag<=13){
                    if(temp<dis && temp < thisdis){
                        thisdis = temp;
                        thistarget = thisenemy;
                    }

                }
                else if(results[j].collider.tag == 0){
                    if(temp < closest){
                        closest = temp;
                        collide = thisenemy.name;
                    }

                }  
            } 
            if(thisdis < dis && closest > thisdis){
                dis = thisdis;
                target = thistarget;
                pos = v1;

            }
            
        }
        var avail;
        if(this.player)avail = this.node.getChildByName('knight1').getComponent("playermove").usemp(1);
        else avail = 1;
        if(avail){
            this.gun.getComponent(cc.Animation).play("player_gun_fire");
            if(this.gun2.active)this.gun2.getComponent(cc.Animation).play("player_gun_fire");
            if(this.player)cc.audioEngine.playEffect(this.wizardbullet, false);
            else cc.audioEngine.playEffect(this.knight_fire, false);
            this.scheduleOnce(function() {
                if(dis>=1000 || 1){
                    this.spawn(event);
                }
                else{
                    cc.log("a");
                    if(target.active == true && target!= null && target.name!=''){
                        var pos = target.convertToWorldSpaceAR(cc.v2(0,0));
                        this.shooting(pos.x,pos.y);
                    }
                }
            }, 0.2);
        }
    }

    positionXY(event){
        var mousePosition = event.getLocation().sub(cc.v2(this.node.width/2,this.node.height/2));
        let angle = Math.atan(mousePosition.y/mousePosition.x)*180/Math.PI;
        if(mousePosition.x < 0 ){
            angle = -angle;
            this.node.getChildByName('knight1').scaleX = -2.408;
        }
        else{
            this.node.getChildByName('knight1').scaleX = 2.408;
        }
        this.gun.angle = angle;
        if(this.gun2.active)this.gun2.angle = angle;
    }

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
       // cc.find("Canvas/Main Camera/weapon_bg").getComponent("weapon").changeWeapon();
    }
    shootingevent(event){
        if(cc.find("Canvas/knight1").active && !cc.find('Canvas/knight1').getComponent('playermove').frozen && !cc.find('Canvas/knight1').getComponent('playermove').stop && !cc.find("Canvas/knight1/player_rainbow_horse").active){
            if(cc.find("Canvas/knight1/magic_wand").active){
                this.magicball(event);
            }
            else{
                this.autoshooting2(event);
            }
        }
        if(cc.find('Canvas/knight1').getComponent('playermove').before_room=='0')event.stopPropagation();
    }

    start () {
        this.player=cc.director.getScene().getChildByName('global').getComponent('persistnode').player;
        if(this.player){
            this.bullet = this.wizard_bullet;
        }
        else this.bullet = this.knight_bullet;
        this.node.getChildByName('contact').on('mousemove',this.positionXY,this);
        this.node.on('mousedown',this.shootingevent,this);
    }

  
}

