const {ccclass, property} = cc._decorator;

@ccclass
export default class coin_mountain extends cc.Component {
    anim = null;

    isdead: boolean = false;

    mountainHP: number = 10;

    @property(cc.Prefab)
    coinPrefab: cc.Prefab = null;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    update(dt) {
        if(!this.isdead && this.mountainHP <= 0) {
            this.coinMountainDead();
        }
    }

    coinMountainDead() {
        this.isdead = true;
        this.anim.play("coin_mountain_dead");
            
        // 20 coins
        for(var i = 0; i < 20; i++) {
            let coin = cc.instantiate(this.coinPrefab);
            coin.parent = this.node;
            coin.setPosition(Math.random()*100-50, Math.random()*100-50);
        }
    }

    onBeginContact(contact, self, other) {
        if(this.isdead) {
            contact.disabled = true;
        }
        else {
            if(other.tag == 5) { // bullet
                this.mountainHP -= 1;
            }
            else if(other.tag == 6) { // rainbow
                this.mountainHP -= 0.01;
            }
        }
    }
}
