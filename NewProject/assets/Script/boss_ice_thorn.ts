const {ccclass, property} = cc._decorator;

@ccclass
export default class boss_ice_thorn extends cc.Component {
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 0) { // wall
            this.node.destroy();
        }
        else if(other.tag == 1) { // player
            this.node.destroy();
        }
    }

}
