const {ccclass, property} = cc._decorator;

@ccclass
export default class player_rainbow_horse extends cc.Component {
    angle: number;

    mouseDown: boolean = false;

    mouseX: number = 1;

    mouseY: number = 0;

    ranbowWidth: number = 200;

    rainbowHorse = null;

    rainbow = null;

    rainbowHorse2 = null;

    rainbow2 = null;

    state: number = 0;

    interval: number = 0;

    frozen: boolean = false;

    frozenTime: number = 0;

    openSkill: boolean = false;

    @property(cc.Prefab)
    private emptyPrefab: cc.Prefab = null;

    @property({type:cc.AudioClip})
    rainbowSound: cc.AudioClip = null;

    playEffect(sound){
        cc.audioEngine.playEffect(sound, false);
    }

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        //cc.director.getPhysicsManager().debugDrawFlags = 1;

        this.node.on(cc.Node.EventType.MOUSE_DOWN, function(event) {
            this.mouseDown = true;
        }, this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE, this.onMouseMove, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, function() {
            this.mouseDown = false;
            this.rainbow.active = false;
        }, this);

        this.rainbowHorse = cc.find("Canvas/knight1/player_rainbow_horse");
        this.rainbow = cc.find("Canvas/knight1/player_rainbow_horse/player_rainbow");
    }

    update(dt) {
        if(this.rainbowHorse.active) {
            if(this.frozen) {
                this.rainbow.active = false;
                this.frozenTime += dt;
                if(this.frozenTime >= 2) {
                    this.frozenTime = 0;
                    this.frozen = false;
                }
                return;
            }

            if(this.openSkill) {
                cc.find("Canvas/knight1/player_rainbow_horse").getComponent(cc.Animation).play("rainbow_horse_2");
                cc.find("Canvas/map/boss").getComponent("boss").rainbowAttack = 0.02;
                this.rainbow.height = 20;
            }
            else {
                cc.find("Canvas/knight1/player_rainbow_horse").getComponent(cc.Animation).play("rainbow_horse");
                cc.find("Canvas/map/boss").getComponent("boss").rainbowAttack = 0.01;
                this.rainbow.height = 12;
            }

            if(cc.find("Canvas/knight1").getComponent("playermove").MP <= 0) {
                this.rainbow.active = false;
            }

            this.angle = Math.atan(this.mouseY/this.mouseX)*180/Math.PI;
            if(this.mouseX < 0) 
                this.angle += 180;
            
            this.rainbow.angle = 0; 
            this.rainbow.x = 17;
            this.rainbow.y = 0;

            if(cc.find("Canvas/knight1").scaleX > 0) {
                this.rainbowHorse.angle = this.angle; 
            }
            else {
                this.rainbowHorse.angle = 180-this.angle; 
            }

            if(this.mouseDown && (cc.find("Canvas/knight1").getComponent("playermove").MP > 0)) {
                this.measureWidth(cc.v2(cc.find("Canvas/Main Camera").x,cc.find("Canvas/Main Camera").y), 0);

                this.interval += dt;
                if(this.interval > 0.2) {
                    this.interval = 0;

                    //mp-1
                    cc.find("Canvas/knight1").getComponent("playermove").usemp(1);
                    this.playEffect(this.rainbowSound);
                }
            }
            else {
                this.interval = 0;
            }
        }
    }

    onMouseMove(event) {
        this.mouseX = event.getLocationX()-480;
        this.mouseY = event.getLocationY()-320;
    }

    createRainbow(pos: cc.Vec2) {
        if(this.mouseDown) 
            this.rainbow.active = true;

        this.rainbow.width = this.ranbowWidth;
    }

    measureWidth(last_pos: cc.Vec2, i: number) {
        let bullet = cc.instantiate(this.emptyPrefab);
        bullet.parent = cc.find("Canvas");

        bullet.position = cc.v2(last_pos.x+Math.cos(this.angle*Math.PI/180)*15, last_pos.y+Math.sin(this.angle*Math.PI/180)*15);
        bullet.angle = this.angle;

        this.scheduleOnce(function() {
            if(bullet.active && i <= 100)
                this.measureWidth(bullet.position, i+1);
            else {
                this.ranbowWidth = (i-2)*15;
                this.createRainbow(cc.v2(cc.find("Canvas/Main Camera").x+this.rainbowHorse.x,cc.find("Canvas/Main Camera").y+this.rainbowHorse.y));
            }
        }, 0.001);
        
        this.scheduleOnce(function() {
            if(bullet.active)
                bullet.destroy();
        }, 0.05);
    }
}
