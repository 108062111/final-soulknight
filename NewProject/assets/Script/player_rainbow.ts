const {ccclass, property} = cc._decorator;

@ccclass
export default class player_rainbow extends cc.Component {
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        //cc.director.getPhysicsManager().debugDrawFlags = 1;
    }

    onBeginContact(contact, self, other) {
        if(self.node.name == "empty") {
            if(other.tag == 0) { // wall
                this.node.destroy();
            }
            else {
                contact.disabled = true;
            }
        }
    }

}
