const {ccclass, property} = cc._decorator;

@ccclass
export default class small_room extends cc.Component {

    dead_num: number = 0;
    first_enter: boolean = false;

    @property({type:cc.AudioClip})
    bossbgm: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;
    @property({type:cc.AudioClip})
    fenceeffect: cc.AudioClip = null;

    untravel : cc.Color = cc.color(60, 60, 60);
    now: cc.Color = cc.color(255, 255, 255);
    traveled : cc.Color = cc.color(150, 150, 150);
    
    nothing: number = 0;
    /*測雪人進場動畫用的
    start(){
        
        this.scheduleOnce(function() {
            cc.find("map_Mgr").getComponent("map_Mgr").enter_boss_room = true;
        }, 1);
    }
    */
    //fence升起來
    fence_up(num){
        cc.find("Canvas").getComponent("playershoot").auto = 1;
        cc.find("Canvas/map/fence"+ num).active = true;
        cc.audioEngine.playEffect(this.fenceeffect, false);
        cc.find("Canvas/map/fence"+num+"/fence0").getComponent(cc.Animation).play('fence_hor_up');
        cc.find("Canvas/map/fence"+num+"/fence4").getComponent(cc.Animation).play('fence_ver_up');
    }
    fence_down(num){
        cc.find("Canvas").getComponent("playershoot").auto = 0;
       
        this.scheduleOnce(function() {
            cc.find("Canvas/map/fence"+ num).active = false;
        }, 0.3);
        cc.audioEngine.playEffect(this.fenceeffect, false);
        cc.find("Canvas/map/fence"+num+"/fence0").getComponent(cc.Animation).play('fence_hor_down');
        cc.find("Canvas/map/fence"+num+"/fence4").getComponent(cc.Animation).play('fence_ver_down');
    }
    update(dt){
        if(this.node.name == "2"){
            // cc.log("room2 的dead_num = "+ this.dead_num);
            if(this.dead_num == 5){
                this.dead_num++;
                this.fence_down("2");
            }
        }
        else if(this.node.name == "3"){
            if(this.dead_num == 5){
                this.dead_num++;
                this.fence_down("3");
            }
        }
        else if(this.node.name == "5"){
            if(this.dead_num == 5){
                this.dead_num++;
                this.fence_down("5");
            }
        }
        else if(this.node.name == "7"){
            if(this.dead_num == 1){
                this.dead_num++;
                cc.audioEngine.playMusic(this.bgm, true);
                this.fence_down("7");
            }
        }
    }
    onBeginContact(contact, self, other) {
        contact.disabled = true;
        if(this.node.name == "0"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "0";
                cc.find("Canvas/Main Camera/small_map/0/room").color = this.now;
            }
        } 
        
        else if(this.node.name == "1"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "1";
                cc.find("Canvas/Main Camera/small_map/1/room").color = this.now;
                //要show的
                if(cc.find("Canvas/Main Camera/small_map/4/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/4/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/4/pic").active = true;
                    cc.find("Canvas/Main Camera/small_map/4/room").color = this.untravel;
                }
                cc.find("Canvas/Main Camera/small_map/path1-4").active = true;
            }
        } else if(this.node.name == "2"){
            if(other.node.name == "knight1"){
                
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "2";
                cc.find("Canvas/Main Camera/small_map/2/room").color = this.now;
                //看要不要升fence
                if(this.first_enter == false){
                    this.first_enter = true;
                    this.fence_up("2");
                    /*測試fence_down用
                    this.scheduleOnce(function() {
                        this.dead_num = 5;
                    }, 3);*/
                }
                //要show的
                cc.find("Canvas/Main Camera/small_map/2/path").active = true;
                if(cc.find("Canvas/Main Camera/small_map/1/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/1/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/1/room").color = this.untravel;
                    cc.find("Canvas/Main Camera/small_map/1/pic").active = true;
                }
                if(cc.find("Canvas/Main Camera/small_map/3/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/3/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/3/room").color = this.untravel;
                }
                if(cc.find("Canvas/Main Camera/small_map/5/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/5/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/5/room").color = this.untravel;
                }
            }
        } else if(this.node.name == "3"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "3";
                cc.find("Canvas/Main Camera/small_map/3/room").color = this.now;
                //看要不要升fence
                if(this.first_enter == false){
                    this.first_enter = true;
                    this.fence_up("3");
                    /*測試fence_down用
                    this.scheduleOnce(function() {
                        this.dead_num = 5;
                    }, 3);*/
                }
            }
        } else if(this.node.name == "4"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "4";
                cc.find("Canvas/Main Camera/small_map/4/room").color = this.now;
                //要show的
                cc.find("Canvas/Main Camera/small_map/path").active = true;
                if(cc.find("Canvas/Main Camera/small_map/5/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/5/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/5/room").color = this.untravel;
                }
                cc.find("Canvas/Main Camera/small_map/path1-4").active = true;
            }
        } else if(this.node.name == "5"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "5";
                cc.find("Canvas/Main Camera/small_map/5/room").color = this.now;
                //要show的
                cc.find("Canvas/Main Camera/small_map/path").active = true;
                cc.find("Canvas/Main Camera/small_map/5/path").active = true;
                if(cc.find("Canvas/Main Camera/small_map/4/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/4/pic").active = true;
                    cc.find("Canvas/Main Camera/small_map/4/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/4/room").color = this.untravel;
                }
                if(cc.find("Canvas/Main Camera/small_map/6/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/6/pic").active = true;
                    cc.find("Canvas/Main Camera/small_map/6/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/6/room").color = this.untravel;
                }
                //看要不要升fence
                if(this.first_enter == false){
                    this.first_enter = true;
                    this.fence_up("5");
                    /*測試fence_down用
                    this.scheduleOnce(function() {
                        this.dead_num = 5;
                    }, 3);*/
                }
            }
        } else if(this.node.name == "6"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "6";
                cc.find("Canvas/Main Camera/small_map/6/room").color = this.now;
                //要show的
                cc.find("Canvas/Main Camera/small_map/6/path").active = true;
                if(cc.find("Canvas/Main Camera/small_map/7/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/7/pic").active = true;
                    cc.find("Canvas/Main Camera/small_map/7/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/7/room").color = this.untravel;
                }
            }
        } else if(this.node.name == "7"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "7";
                cc.find("Canvas/Main Camera/small_map/7/room").color = this.now;
                //看要不要播動畫
                if(this.first_enter == false){
                    cc.audioEngine.playMusic(this.bossbgm, true);
                    cc.find("map_Mgr").getComponent("map_Mgr").enter_boss_room = true;
                    this.first_enter = true;
                    
                    this.fence_up("7");
                    /*測試fence_down用
                    this.scheduleOnce(function() {
                        this.dead_num = 1;
                    }, 6);*/
                }
                //要show的
                cc.find("Canvas/Main Camera/small_map/7/path").active = true;
                if(cc.find("Canvas/Main Camera/small_map/8/room").active == false){
                    cc.find("Canvas/Main Camera/small_map/8/pic").active = true;
                    cc.find("Canvas/Main Camera/small_map/8/room").active = true;
                    cc.find("Canvas/Main Camera/small_map/8/room").color = this.untravel;
                }

                this.scheduleOnce(function() {
                    cc.find("Canvas/map/boss").getComponent("boss").enter = true;
                }, 2);
                if(cc.find("Canvas/map/boss").getComponent("boss").bossState != -1)
                    cc.find("Canvas/Main Camera/boss_hp").active = true; //show boss hp bar
            }
        } else if(this.node.name == "8"){
            if(other.node.name == "knight1"){
                cc.find("Canvas/Main Camera/small_map/"+other.node.getComponent("playermove").before_room+"/room").color = this.traveled;
                other.node.getComponent("playermove").before_room = "8";
                cc.find("Canvas/Main Camera/small_map/8/room").color = this.now;
            }
        } 
    }
}
