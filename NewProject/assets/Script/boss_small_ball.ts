const {ccclass, property} = cc._decorator;

@ccclass
export default class boss_small_ball extends cc.Component {
    anim = null;

    num: number;
    
    @property(cc.Prefab)
    private icePrefab: cc.Prefab = null;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    public init(node: cc.Node, n: number) 
    {
        this.num = n;
        this.setInitPos(node);
        this.bulletMove();
    }

    private setInitPos(node: cc.Node)
    {
        this.node.parent = node.parent;

        if(this.num%2 == 0) {
            this.node.position = cc.v2(node.position.x, node.position.y);
            this.node.angle = this.num*10;
        }
        else if(this.num%2 == 1) {
            this.node.position = cc.v2(node.position.x, node.position.y);
            this.node.angle = (this.num-1)*10+180;
        }
    }

    private bulletMove() {
        let x = Math.cos(this.node.angle*Math.PI/180)*100;
        let y = Math.sin(this.node.angle*Math.PI/180)*100;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(x,y);
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 0) { // wall
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);

            this.anim.play("boss_small_ball_dead");
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
        else if(other.tag == 1) { // player
            let ice = cc.instantiate(this.icePrefab);
            ice.parent = cc.find("Canvas");
            ice.setPosition(cc.find("Canvas/knight1").position);
            cc.find("Canvas").getComponent("player_rainbow_horse").frozen = true;

            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);
            this.node.destroy();
        }
    }

}
