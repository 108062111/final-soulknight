const {ccclass, property} = cc._decorator;

@ccclass
export default class menu_Mgr extends cc.Component {
    
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    buttoneffect: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    entereffect: cc.AudioClip = null;

    onLoad(){
        cc.audioEngine.playMusic(this.bgm, true);
    }
    start () {
        let fade = cc.fadeIn(1);
        cc.find("Canvas/scarf").runAction(fade);
        let show = cc.moveBy(1, 0, 120);
        let show2 = cc.moveBy(1, 0, 120);
        cc.find("Canvas/logIn_b").runAction(show);
        cc.find("Canvas/signUp_b").runAction(show2);
    }
    
    //按下logIn button
    Button_log (event, customEventData) {
        cc.log("press login button");
        cc.find("Canvas/LogIn").active = true;
        cc.find("Canvas/logIn_b").active = false;
        cc.find("Canvas/signUp_b").active = false;
        cc.audioEngine.playEffect(this.buttoneffect, false);
    }
    //按下SignUp button
    Button_sign (event, customEventData) {
        cc.log("press sign button");
        cc.find("Canvas/SignUp").active = true;
        cc.find("Canvas/logIn_b").active = false;
        cc.find("Canvas/signUp_b").active = false;
        cc.audioEngine.playEffect(this.buttoneffect, false);
    }
    //LOG IN的enter
    Button_log_enter (event, customEventData) {
        cc.log("press login's enter");
        let Email0: string = cc.find("Canvas/LogIn/email").getComponent(cc.EditBox).string;
        let Password: string = cc.find("Canvas/LogIn/password").getComponent(cc.EditBox).string;
        cc.audioEngine.playEffect(this.entereffect, false);
        //登入
        firebase.auth().signInWithEmailAndPassword(Email0, Password).then(function(){
            cc.log("log in success");
            //todo 要換場景
            cc.director.loadScene("Select");
        }).catch(function(error) {
            var errorMessage = error.message;
            alert(errorMessage);
            cc.log("log in error");
            cc.find("Canvas/LogIn/email").getComponent(cc.EditBox).string = "";
            cc.find("Canvas/LogIn/password").getComponent(cc.EditBox).string = "";
        });
    }
    //LOG IN的close
    Button_log_close (event, customEventData) {
        cc.log("press login's close");
        cc.find("Canvas/LogIn/email").getComponent(cc.EditBox).string = "";
        cc.find("Canvas/LogIn/password").getComponent(cc.EditBox).string = "";
        cc.find("Canvas/LogIn").active = false;
        cc.find("Canvas/logIn_b").active = true;
        cc.find("Canvas/signUp_b").active = true;
        cc.audioEngine.playEffect(this.buttoneffect, false);
    }
    //SIGN UP的enter
    Button_sign_enter (event, customEventData) {
        cc.log("press sign's enter");

        let Email0: string = cc.find("Canvas/SignUp/email").getComponent(cc.EditBox).string;
        let Username: string = cc.find("Canvas/SignUp/username").getComponent(cc.EditBox).string;
        let Password: string = cc.find("Canvas/SignUp/password").getComponent(cc.EditBox).string;
        let Email: string = cc.find("Canvas/SignUp/email").getComponent(cc.EditBox).string.replace(/\./gi,"");
        cc.audioEngine.playEffect(this.entereffect, false);
        
        ///註冊+登入
        firebase.auth().createUserWithEmailAndPassword(Email0,Password).then(function(success) {
            cc.log("sign up success");
            cc.log("email: " + Email);
            cc.log("username: " + Username);
            cc.log("password: " + Password);
            
            //在firebase開一個用他的email名的資料夾，裡面放username
            var data = {
                email: Email,
                username: Username, 
                record: 0, 
                time: 0, 
                year: 0, 
                month: 0, 
                date: 0, 
                playerstate: 0, 
                record_playerstate: 0
            };
            firebase.database().ref('user').child(Email).set(data);
            
            //登入
            firebase.auth().signInWithEmailAndPassword(Email0, Password).then(function(){
                cc.log("log in success");
                //todo 要換場景
                cc.director.loadScene("Select");
            }).catch(function(error) {
                cc.log("log in error");
            });
        }).catch(function(error) {
            // Handle Errors here.
            var errorMessage = error.message;
            alert(errorMessage);
            cc.log("sign up error");
            cc.find("Canvas/SignUp/email").getComponent(cc.EditBox).string = "";
            cc.find("Canvas/SignUp/username").getComponent(cc.EditBox).string = "";
            cc.find("Canvas/SignUp/password").getComponent(cc.EditBox).string = "";
        });
    }
    //SIGN UP的close
    Button_sign_close (event, customEventData) {
        cc.log("press sign's close");
        cc.find("Canvas/SignUp/email").getComponent(cc.EditBox).string = "";
        cc.find("Canvas/SignUp/username").getComponent(cc.EditBox).string = "";
        cc.find("Canvas/SignUp/password").getComponent(cc.EditBox).string = "";
        cc.find("Canvas/SignUp").active = false;
        cc.find("Canvas/logIn_b").active = true;
        cc.find("Canvas/signUp_b").active = true;
        cc.audioEngine.playEffect(this.buttoneffect, false);
    }
}
