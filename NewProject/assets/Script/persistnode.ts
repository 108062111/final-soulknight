
const {ccclass, property} = cc._decorator;

@ccclass
export default class persistnode extends cc.Component {

    @property
    player: number = 0;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.parent = null;
        cc.game.addPersistRootNode(this.node);
    }

    

    // update (dt) {}
}