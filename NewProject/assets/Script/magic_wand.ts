const {ccclass, property} = cc._decorator;

@ccclass
export default class magic_wand extends cc.Component {
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.node.on(cc.Node.EventType.MOUSE_DOWN, function(event){
            if(this.node.getChildByName("magic_wand_text").active) {
                cc.find("Canvas/Main Camera/weapon_bg").getComponent("weapon").addWeapon(3);
                this.node.parent.getComponent('box').taken = true;
                this.node.destroy();
                event.stopPropagation()
            }
        }, this);
    }

  
}
