

import AStar from "./AStar";
import MapCell, { ECellType } from "./MapCell";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Open extends cc.Component {

    @property(cc.Prefab)
    eCell: cc.Prefab = null;

    @property(cc.Node)
    eLayout: cc.Node = null;

    @property
    myWidth: number = 1;

    @property
    myHeight: number = 1;

    @property
    startX: number = 1;

    @property
    startY: number = 1;

    private star = new AStar();
    private mCells = {};

    private mSize: cc.Vec2 = null;
    private mStart: cc.Vec2 = null;
    private mEnd: cc.Vec2 = null;
    private mObstacles: cc.Vec2[] = [];
    onLoad () {
        console.log("new3");
        const size = cc.v2(this.myWidth, this.myHeight);
        const start = cc.v2(0, 0);
        const end = cc.v2(this.myWidth-1, this.myHeight-1);
        const obstacles = [
            cc.v2(4,4),
            cc.v2(4,5),
            cc.v2(4,6),
            cc.v2(4,7),
            cc.v2(4,8),
            cc.v2(4,9),
            cc.v2(5,6),
            cc.v2(5,7),
            cc.v2(6,6),
            cc.v2(6,7),
            cc.v2(7,6),
            cc.v2(7,7),
            cc.v2(8,6),
            cc.v2(8,7),
            cc.v2(8,8),
            cc.v2(8,9),
            cc.v2(8,10),
            cc.v2(8,11),
            cc.v2(9,2),
            cc.v2(9,3),
            cc.v2(9,10),
            cc.v2(9,11),
            cc.v2(10,2),
            cc.v2(10,3),
            cc.v2(10,10),
            cc.v2(10,11),
            cc.v2(11,2),
            cc.v2(11,3),
            cc.v2(11,10),
            cc.v2(11,11),
            cc.v2(12,2),
            cc.v2(12,3),
            cc.v2(12,4),
            cc.v2(12,5),
            cc.v2(12,6),
            cc.v2(12,7),
            cc.v2(13,6),
            cc.v2(13,7),
            cc.v2(14,6),
            cc.v2(14,7),
            cc.v2(15,6),
            cc.v2(15,7),
            cc.v2(16,4),
            cc.v2(16,5),
            cc.v2(16,6),
            cc.v2(16,7),
            cc.v2(16,8),
            cc.v2(16,9)            
        ];

        this.init(size, start, end, obstacles);
    }

    init(size: cc.Vec2, start: cc.Vec2, end: cc.Vec2, obstacles: cc.Vec2[] = []) {
        this.mSize = size;
        this.mStart = start;
        this.mEnd = end;
        this.mObstacles = obstacles;
        
        this.initData();
        this.initUI();
    }
    
    initData() {
        this.star.init(this.mSize, this.mStart, this.mEnd, this.mObstacles);
    }
    
    initUI() {
        
        this.mCells = {};
        this.eLayout.removeAllChildren();
        for (let y = 0; y < this.mSize.y; y++){
            for (let x = 0; x < this.mSize.x; x++) {
                this.createCell(x, y);
            }
        }
    
        this.mObstacles.forEach((ele) => {
            this.setCell(ele.x, ele.y, ECellType.OBSTACLES);
        });
        const path = this.star.getClose();
        path.forEach((ele) => {
            this.setCell(ele.x, ele.y, ECellType.PATH);
        });
        this.setCell(this.mStart.x, this.mStart.y, ECellType.START);
        this.setCell(this.mEnd.x, this.mEnd.y, ECellType.END);
    
        // this.eLayout.width = 2 + (50 + 2) * this.mSize.x;
        // if (this.mSize.x > 10) {
        //     this.eLayout.scale = 10 / this.mSize.x;
        // } else if (this.mSize.y > 10) {
        //     this.eLayout.scale = 10 / this.mSize.y;
        // }
    }

    // 创建cell
    createCell(x: number, y: number) {
        const node = cc.instantiate(this.eCell);
        node.parent = this.eLayout;
        this.mCells[`${x}_${y}`] = node.getComponent(MapCell);
        this.mCells[`${x}_${y}`].init(x, y);
    }

    // 设置cell类型
    setCell(x: number, y: number, type: ECellType) {
        this.mCells[`${x}_${y}`].setType(type);
    }

    // 点击进行一次单步寻路
    // onClickNext() {
    //     this.star.next();
    //     this.initUI();
    // }

    // 点击进行执行寻路
    onClickRun() {
        this.star.run();
        this.initUI();
    }

    updateStart(cell: cc.Vec2) {
        this.setCell(this.mStart.x, this.mStart.y, ECellType.PATH);
        this.setCell(cell.x, cell.y, ECellType.START);
        this.mStart = cell;
    }

    updateEnd(cell: cc.Vec2) {
        this.setCell(this.mEnd.x, this.mEnd.y, ECellType.PATH);
        this.setCell(cell.x, cell.y, ECellType.END);
        this.mEnd = cell;
    }
}
