const {ccclass, property} = cc._decorator;

@ccclass

export default class box extends cc.Component {
    @property({type:cc.Prefab})
    wand: cc.Prefab = null;
    open:boolean;
    taken:boolean;
    @property({type:cc.AudioClip})//開寶箱: open_box
    open_box: cc.AudioClip = null;
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.open = false;
        this.taken = false;
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 1 ) { // player
            if(!this.open){
                this.node.getComponent(cc.Animation).play('box_open');
                cc.audioEngine.playEffect(this.open_box, false);
                var magicw = cc.instantiate(this.wand);
                magicw.setPosition(cc.v2(0,0));
                magicw.rotation = 90;
                magicw.parent = this.node;
                this.open = true;
            }
            if(!this.taken){
                this.node.getChildByName('magic_wand').getChildByName("magic_wand_text").active = true;
            }
        }
    }

    onEndContact(contact, self, other) {
        if(other.tag == 1 && !this.taken) { // player
            this.node.getChildByName('magic_wand').getChildByName("magic_wand_text").active = false;
        }
    }
}
