const {ccclass, property} = cc._decorator;

@ccclass
export default class energy_bottle extends cc.Component {
    isbuy: boolean = false;

    @property({type:cc.AudioClip})
    buySound: cc.AudioClip = null;

    coin_num: cc.Label = null;

    cost: number = 24;

    discription: string = "<color=#ffffff>能量藥水</c> <color=#ffff00>24</color>";

    alert: string = "餘額不足";

    playEffect(sound){
        cc.audioEngine.playEffect(sound, false);
    }

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.coin_num = cc.find("Canvas/Main Camera/coin/coin_num").getComponent(cc.Label);
        this.node.on(cc.Node.EventType.MOUSE_DOWN, function(event){
            if(this.node.getChildByName("energy_bottle_text").active && parseInt(this.coin_num.string) >= this.cost) {
                this.coin_num.string = (parseInt(this.coin_num.string) - this.cost).toString();

                this.isbuy = true;
                this.node.getChildByName("energy_bottle_text").active = false;
                this.node.removeComponent(cc.Sprite);
                cc.find('Canvas/knight1').getComponent('playermove').drinkenergy();
                this.playEffect(this.buySound);
                event.stopPropagation();
            }
            else{
                this.node.getChildByName("energy_bottle_text").getComponent(cc.RichText).string = this.alert;
                this.scheduleOnce(function() {
                    this.node.getChildByName("energy_bottle_text").getComponent(cc.RichText).string = this.discription;
                }, 1);
            }
        }, this);
    }

    onBeginContact(contact, self, other) {
        if(other.tag == 1) { // player
            if(contact.getWorldManifold().normal.x == 0 && contact.getWorldManifold().normal.y == -1 ||
               contact.getWorldManifold().normal.x == 0 && contact.getWorldManifold().normal.y == 1) {
                if(!this.isbuy)
                    this.node.getChildByName("energy_bottle_text").active = true;
            }
        }
    }

    onEndContact(contact, self, other) {
        if(other.tag == 1) { // player
            this.node.getChildByName("energy_bottle_text").active = false;
        }
    }
}
