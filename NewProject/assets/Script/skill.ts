const {ccclass, property} = cc._decorator;

@ccclass
export default class skill extends cc.Component {
    skillDown: boolean = false; // key for skill

    skillHeight: number = 0;

    skillAdd: number = 0.3; // speed

    skillState: number = 0; /* 0:off, 1:full, 2:on */

    iceState: number = 0;

    skillFullColor: cc.Color = cc.Color.CYAN;

    playerState: string = "1";

    @property(cc.Prefab)
    private smallIcePrefab: cc.Prefab = null;

    @property(cc.Node)
    gun2: cc.Node = null;

    @property({type:cc.AudioClip})
    knightSkillSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    wizardSkillSound: cc.AudioClip = null;

    playEffect(sound){
        cc.audioEngine.playEffect(sound, false);
    }


    onLoad () {
        this.skillHeight = cc.find("Canvas/Main Camera/skill_bg/skill").height;
        //this.playerState = cc.find("Canvas/Main Camera/fail/playerstate").getComponent(cc.Label).string;
        this.playerState = cc.director.getScene().getChildByName('global').getComponent('persistnode').player;

        this.node.parent.on(cc.Node.EventType.MOUSE_DOWN, function(event){
            this.skillDown = true;
        }, this);
        this.node.parent.on(cc.Node.EventType.MOUSE_UP, function(event){
            this.skillDown = false;
        }, this);
    }

    update(dt) {
        if(this.skillState == 0) {
            this.gun2.active = false;
            cc.find("Canvas/magicball/magic_ball pink").active = false;
            if(cc.find("Canvas/knight1/player_rainbow_horse").active) {
                cc.find("Canvas").getComponent("player_rainbow_horse").openSkill = false;
            }
            if(cc.find("Canvas/Main Camera/skill_bg/skill_mask").height >= this.skillHeight*2) { // full
                this.skillState = 1;
                cc.find("Canvas/Main Camera/skill_bg/skill_mask/skill_white").color = this.skillFullColor;
            }
            else {
                cc.find("Canvas/Main Camera/skill_bg/skill_mask").height += this.skillAdd;
            }   
        }
        else if(this.skillState == 1) {
            if(this.skillDown) {
                cc.find("Canvas/Main Camera/skill_bg/skill_mask/skill_white").color = cc.Color.WHITE;
                
                if(this.playerState == "0") { // knight
                    this.skillState = 2;
                    // TODO:發動技能
                    if(cc.find("Canvas/knight1/player_rainbow_horse").active) {
                        cc.find("Canvas").getComponent("player_rainbow_horse").openSkill = true;
                    }
                    else if(cc.find("Canvas/knight1/player_gun1").active)this.gun2.active = true;
                    else if(cc.find("Canvas/knight1/magic_wand").active) {
                        cc.find("Canvas/magicball/magic_ball pink").active = true;
                    }
                    this.playEffect(this.knightSkillSound);
                }
                else { // wizard
                    if(this.iceState == 0) {
                        this.autoshooting2(this.iceState);
                        cc.find("Canvas/Main Camera/skill_bg/skill_mask").height = 0;
                        this.iceState += 1;
                        this.playEffect(this.wizardSkillSound);
                    }
                }
                
            }
            else {
                if(this.playerState == "1") { // wizard
                    if(this.iceState == 1) {
                        this.autoshooting2(this.iceState);
                        cc.find("Canvas/Main Camera/skill_bg/skill_mask").height = 0;
                        this.iceState += 1;
                    }
                    else if(this.iceState == 2) {
                        this.autoshooting2(this.iceState);
                        this.iceState = 0;
                        this.skillState = 0;
                    }
                }
            }
        }
        else if(this.skillState == 2) {
            if(cc.find("Canvas/Main Camera/skill_bg/skill_mask").height <= 0) {
                this.skillState = 0;
            }
            else {
                cc.find("Canvas/Main Camera/skill_bg/skill_mask").height -= this.skillAdd;
            }
        }
    }

    createIce(last_pos: cc.Vec2, i: number, dir: cc.Vec2) {
        let bullet = cc.instantiate(this.smallIcePrefab);
        bullet.parent = cc.find("Canvas");

        bullet.position = cc.v2(last_pos.x+dir.x*30, last_pos.y+dir.y*30);

        this.scheduleOnce(function() {
            if(bullet.active && i <= 6)
                this.createIce(bullet.position, i+1, dir);
        }, 0.05);
        
        this.scheduleOnce(function() {
            if(bullet.active)
                bullet.destroy();
        }, 2);
    }

    autoshooting2(n: number){ 
        var dis = 200000000;
        var target;
        var pos;

        for(let i=360; i>=0; i-=2){  
            let v1 = cc.v2(Math.cos(Math.PI * i / 180)*1000,Math.sin(Math.PI * i / 180)*1000);  
            let v2 = cc.find("Canvas/knight1").convertToWorldSpaceAR(v1);  
            let results = cc.director.getPhysicsManager().rayCast(cc.find("Canvas/knight1").convertToWorldSpaceAR(cc.v2(0,0)) ,v2,cc.RayCastType.All);  
            var closest = 200000000;
            var thisdis = 200000000;
            var thistarget;
            for(let j=0; j<results.length; j++){  
                var thisenemy = results[j].collider.node;
                var thisenemypos=  results[j].collider.node.parent.convertToWorldSpaceAR(cc.v2(results[j].collider.node.x,results[j].collider.node.y));    
                var temp = Math.abs(thisenemypos.sub(cc.find("Canvas").convertToWorldSpaceAR(cc.find("Canvas/knight1").position)).mag());
                if(results[j].collider.tag>=10 && results[j].collider.tag<=13){
                    if(temp < dis && temp < thisdis){
                        thisdis = temp;
                        thistarget = thisenemy;
                    }
                }

            } 
            if(thisdis < dis && closest > thisdis){
                dis = thisdis;
                target = thistarget;
                pos = v1;
            }
        }

        this.scheduleOnce(function() {
            if(dis>=1000){
                var dir = cc.v2(1,0);
                if(cc.find("Canvas/knight1").scaleX < 0)
                    dir = cc.v2(-1,0);

                if(n == 0)
                    this.createIce(cc.find("Canvas/Main Camera").position, 0, dir);
                else if(n == 1) {
                    var rotation_dir = cc.v2(Math.cos(30*Math.PI/180)*dir.x-Math.sin(30*Math.PI/180)*dir.y, Math.sin(30*Math.PI/180)*dir.x+Math.cos(30*Math.PI/180)*dir.y);
                    this.createIce(cc.find("Canvas/Main Camera").position, 0, rotation_dir);
                }
                else if(n == 2) {
                    var rotation_dir = cc.v2(Math.cos(-30*Math.PI/180)*dir.x-Math.sin(-30*Math.PI/180)*dir.y, Math.sin(-30*Math.PI/180)*dir.x+Math.cos(-30*Math.PI/180)*dir.y);
                    this.createIce(cc.find("Canvas/Main Camera").position, 0, rotation_dir);
                }
            }
            else{
                if(target.active == true && target!= null && target.name!=''){
                    var diff = target.convertToWorldSpaceAR(cc.v2(0,0)).sub(cc.find("Canvas/Main Camera").convertToWorldSpaceAR(cc.v2(0,0)));
                    var dir = cc.v2(diff.x/dis,diff.y/dis);
                    if(n == 0)
                        this.createIce(cc.find("Canvas/Main Camera").position, 0, dir);
                    else if(n == 1) {
                        var rotation_dir = cc.v2(Math.cos(30*Math.PI/180)*dir.x-Math.sin(30*Math.PI/180)*dir.y, Math.sin(30*Math.PI/180)*dir.x+Math.cos(30*Math.PI/180)*dir.y);
                        this.createIce(cc.find("Canvas/Main Camera").position, 0, rotation_dir);
                    }
                    else if(n == 2) {
                        var rotation_dir = cc.v2(Math.cos(-30*Math.PI/180)*dir.x-Math.sin(-30*Math.PI/180)*dir.y, Math.sin(-30*Math.PI/180)*dir.x+Math.cos(-30*Math.PI/180)*dir.y);
                        this.createIce(cc.find("Canvas/Main Camera").position, 0, rotation_dir);
                    }
                }
            }
        }, 0.2);
    }
}
