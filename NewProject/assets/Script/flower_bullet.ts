
const {ccclass, property} = cc._decorator;

@ccclass
export default class flower_bullet extends cc.Component {
    speed: number = 200;
    anim: cc.Animation = null;
    shootrange: number[] = [40, 50];
    state: number = 0; // 0: 100% speed, 1: 20% speed, 2: 0% speed
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.anim = this.getComponent(cc.Animation);
    }

    // start () {
    // }

    init(idx: number, total: number){
        let angle = 360 / total;
        let x = Math.cos(idx*angle*Math.PI/180);
        let y = Math.sin(idx*angle*Math.PI/180);
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(x*this.speed, y*this.speed);
    }

    update (dt) {
        if(this.state == 0 && this.dist(this.node.position, cc.v2(0,0)) > this.shootrange[0]){
            this.state += 1;
            this.node.getComponent(cc.RigidBody).linearVelocity = this.node.getComponent(cc.RigidBody).linearVelocity.mulSelf(0.2);
        }
        else if(this.state == 1 && this.dist(this.node.position, cc.v2(0,0)) > this.shootrange[1]){
            this.state += 1;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);
            this.scheduleOnce(function() {
                this.anim.play("goblin_bullet_dead");
                this.scheduleOnce(function() {
                    this.node.destroy();
                }, 0.5);
            }, 1);
        }
    }

    onBeginContact(contact, self, other) {
        if((other.tag==0)||(other.tag==1) ){
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            this.anim.play("goblin_bullet_dead");
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
    }

    dist(a: cc.Vec2,  b: cc.Vec2){
        let dx = a.x - b.x;
        let dy = a.y - b.y;
        return Math.sqrt(dx*dx+dy*dy);
    }
}
