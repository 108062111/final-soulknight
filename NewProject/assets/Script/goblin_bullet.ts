
const {ccclass, property} = cc._decorator;

@ccclass
export default class goblin_bullet extends cc.Component {
    speed:number=800;
    anim = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.anim = this.getComponent(cc.Animation);
        cc.director.getCollisionManager().enabled=true;
    }
    init(x:number,y:number,angle:number=Math.atan(y/x)*180/Math.PI){
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(x*this.speed, y*this.speed);
      
       // if(x < 0)angle += 180;
        this.node.angle = angle;
       
    }
    update (dt) {
    }
    onBeginContact(contact, self, other) {
        if((other.tag==0)||(other.tag==1) ){
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
            this.anim.play("goblin_bullet_dead");
            this.scheduleOnce(function() {
                this.node.destroy();
            }, 0.5);
        }
    }
    
}
